submodule (bflike_smw_mod) bflike_plms_smod

contains

  module subroutine compute_modes(vals,vecs,blocks,file,lstart)
    implicit none
    !compute legendre (associated) functions and mask them
    type(allocatable_matrix) ,intent(inout) :: vals(2:) ,vecs(2:) ,blocks(2:,2:)
    character(len=*) ,intent(in) ,optional :: file
    integer(i4b)     ,intent(in) ,optional :: lstart

    real(dp) ,allocatable :: plms(:,:) ,maps(:,:,:,:) ,theta(:) ,phi(:) &
         ,y(:,:) ,v(:) ,tm1(:,:) ,tm2(:,:) ,evec(:,:)
    real(dp)              :: sumT ,sumD ,sumS ,xnorm ,vnorm
    
    integer(i4b) :: nsmax ,nlmax ,nmmax ,n_plm ,npol ,nrings ,nchunk ,nl0 &
         ,i ,id ,ipol ,j ,l ,istart ,istop ,nl ,ntheta ,m ,switch ,ntot
    integer(i4b) ,allocatable :: list(:,:) ,nri(:)
    
    nsmax = mask%s%nside
    nlmax = lswitch
    nmmax = lswitch
    n_plm = nsmax*(nmmax +1)*(2*nlmax -nmmax+2)
    nchunk = nlmax +1 +(nlmax*(nlmax+1))/2
    ntot  = mask%ntot
    
    if(has_p) then
       npol = 3
    else
       npol = 1
    end if

    allocate(plms(0:n_plm-1,1:npol))
    call plm_gen( nsmax, nlmax, nmmax, plms)

    !now map them to the sphere...
    
    allocate(theta(0:mask%s%npix-1) ,phi(0:mask%s%npix-1))
    
    do i = 0,mask%s%npix -1 
       call pix2ang_ring(nsmax, i, theta(i), phi(i))
    end do

    nrings = 4*nsmax - 1
    allocate(list(0:nrings,nrings))
    allocate(nri(nrings))
    do i = 1,nrings
       call in_ring(nsmax,i,0._dp,pi,list(:,i),nri(i))
    end do

    allocate(maps(0:mask%s%npix-1,0:nlmax,0:nmmax,1:npol))
    allocate(v(nchunk))

    ntheta = 2*nsmax 
    do ipol = 1,npol
       istart = 0
       if(ipol .eq. 3) then
          switch = -1
       else
          switch = 1
       end if
       do i = 1,ntheta
          istop = istart +(nchunk-1)
          v(1:nchunk) = plms(istart:istop,ipol)
          id    = 0
          do m = 0,nmmax
             do l = m,nlmax
                id = id +1
                maps(list(0:nri(i)-1,i)                  ,l,m,ipol) = v(id)
                maps(list(0:nri(nrings+1-i)-1,nrings+1-i),l,m,ipol) = v(id)*switch*(-1)**(l+m)
             end do
          end do
          istart = istop+1
       end do
    end do
    deallocate(plms,v,nri,list)


    allocate(y(0:mask%s%npix-1,npol) ,source = 0._dp)
    if(has_t) then
       do l = 2,nlmax
          !for each l we store modes in order of m = 0,-1,+1,-2,+2,...,-l,+l
          id = 1
          xnorm = clnorm(l,myTT)*fourpi/(2*l+1_dp)

          !m = 0
          y      = 0._dp
          y(:,1) = maps(:,l,0,1)
          call reorder_map(y,from=1,to=mask%s%ordering)
          v      = map2vec(y)
          call mask_vec(mask%lmask,v)
          vals(l)%m(id,1) = xnorm
          vecs(l)%m(:,id) = v
          
          do m = 1,l
             !negative m
             id     = id+1
             y      = 0._dp
             y(:,1) = sqrt(2._dp)*maps(:,l,m,1)*sin(m*phi)*(-1)**m
             call reorder_map(y,from=1,to=mask%s%ordering)
             v      = map2vec(y)
             call mask_vec(mask%lmask,v)
             vals(l)%m(id,1) = xnorm
             vecs(l)%m(:,id) = v

             !positive m
             id     = id+1
             y      = 0._dp
             y(:,1) = sqrt(2._dp)*maps(:,l,m,1)*cos(m*phi)*(-1)**m
             call reorder_map(y,from=1,to=mask%s%ordering)
             v      = map2vec(y)
             call mask_vec(mask%lmask,v)
             vals(l)%m(id,1) = xnorm
             vecs(l)%m(:,id) = v
          end do
       end do
    end if
    if (has_p) then

       !EE
       do l = 2,nlmax
          nl0 = 2*l+1 
          id  = off*nl0 +1
          xnorm = clnorm(l,myEE)*fourpi/(2*l+1_dp)
          !m = 0
          y      = 0._dp
          y(:,2) =   maps(:,l,0,2)
          y(:,3) =  -maps(:,l,0,3)
          call reorder_map(y,from=1,to=mask%s%ordering)
          v      = map2vec(y)
          call mask_vec(mask%lmask,v)
          vecs(l)%m(:,id) = v
          vals(l)%m(id,1) = xnorm
          blocks(l,myEE)%m(id,id) = vals(l)%m(id,1)
          
          do m = 1,l
             !negative m
             id     = id+1
             y      = 0._dp
             y(:,2) =   sqrt(2._dp)*maps(:,l,m,2)*sin(m*phi)*(-1)**m
             y(:,3) =  -sqrt(2._dp)*maps(:,l,m,3)*cos(m*phi)*(-1)**m
             call reorder_map(y,from=1,to=mask%s%ordering)
             v      = map2vec(y)
             call mask_vec(mask%lmask,v)
             vecs(l)%m(:,id) = v
             vals(l)%m(id,1) = xnorm
             blocks(l,myEE)%m(id,id) = vals(l)%m(id,1)
             
             !positive m
             id     = id+1
             y      = 0._dp
             y(:,2) =   sqrt(2._dp)*maps(:,l,m,2)*cos(m*phi)*(-1)**m
             y(:,3) =   sqrt(2._dp)*maps(:,l,m,3)*sin(m*phi)*(-1)**m
             call reorder_map(y,from=1,to=mask%s%ordering)
             v      = map2vec(y)
             call mask_vec(mask%lmask,v)
             vecs(l)%m(:,id) = v
             vals(l)%m(id,1) = xnorm
             blocks(l,myEE)%m(id,id) = vals(l)%m(id,1)
          end do
       end do
       
     !BB
       do l = 2,nlmax
          nl0 = 2*l+1
          id  = (off+1)*nl0+1
          xnorm = clnorm(l,myBB)*fourpi/(2*l+1_dp)
          !m = 0
          y      = 0._dp
          y(:,2) = maps(:,l,0,3)
          y(:,3) = maps(:,l,0,2)
          call reorder_map(y,from=1,to=mask%s%ordering)
          v      = map2vec(y)
          call mask_vec(mask%lmask,v)
          vecs(l)%m(:,id) = v
          vals(l)%m(id,1) = xnorm
          blocks(l,myBB)%m(id,id) = vals(l)%m(id,1)

          do m = 1,l
             !negative m
             id     = id+1
             y      = 0._dp
             y(:,2) = sqrt(2._dp)*maps(:,l,m,3)*cos(m*phi)*(-1)**m
             y(:,3) = sqrt(2._dp)*maps(:,l,m,2)*sin(m*phi)*(-1)**m
             call reorder_map(y,from=1,to=mask%s%ordering)
             v      = map2vec(y)
             call mask_vec(mask%lmask,v)
             vecs(l)%m(:,id) = v
             vals(l)%m(id,1) = xnorm
             blocks(l,myBB)%m(id,id) = vals(l)%m(id,1)

             !positive m
             id     = id+1
             y      = 0._dp
             y(:,2) = -sqrt(2._dp)*maps(:,l,m,3)*sin(m*phi)*(-1)**m
             y(:,3) = sqrt(2._dp)*maps(:,l,m,2)*cos(m*phi)*(-1)**m
             call reorder_map(y,from=1,to=mask%s%ordering)
             v      = map2vec(y)
             call mask_vec(mask%lmask,v)
             vecs(l)%m(:,id) = v
             vals(l)%m(id,1) = xnorm
             blocks(l,myBB)%m(id,id) = vals(l)%m(id,1)
          end do
       end do
    end if

    deallocate(maps)

    if (has_x) then
       
       do l= 2,nlmax
          nl0 = 2*l+1         
          
          !TE
          blocks(l,myTE)%m = 0._dp
          xnorm = -clnorm(l,myTE)*fourpi/(2*l+1_dp)
          do m = 1,nl0
             blocks(l,myTE)%m(nl0+m,m) = xnorm
             blocks(l,myTE)%m(m,m+nl0) = xnorm
          end do
             
          !TB                                                
          blocks(l,myTB)%m(:,:) = 0._dp
          xnorm = -clnorm(l,myTB)*fourpi/(2*l+1_dp)
          do m = 1,nl0
             blocks(l,myTB)%m(2*nl0+m,m) = xnorm
             blocks(l,myTB)%m(m,m+2*nl0) = xnorm
          end do
       end do

    end if

    if (has_p) then

       !EB                                      
       do l = 2,nlmax
          nl0 = 2*l+1
          nl  = (off+2)*nl0
          xnorm = clnorm(l,myEB)*fourpi/(2*l+1_dp)
          do m = 1,nl0
             blocks(l,myEB)%m(off*nl0+m,(off+1)*nl0+m) = xnorm
             blocks(l,myEB)%m((off+1)*nl0+m,m+off*nl0) = xnorm
          end do
       end do

    end if

    if (present(file)) call write_plms_to_fits(file,clnorm,Tvec,Qvec,Uvec,vals,vecs,blocks)
    
  end subroutine compute_modes
  
end submodule bflike_plms_smod
