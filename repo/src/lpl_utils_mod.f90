module lpl_utils_mod

  !a collection of interfaces to healpix modules and assorted utilities 
  use healpix_types
  use healpix_modules ,only : convert_ring2nest ,convert_nest2ring &
       ,pix2vec_ring ,pix2vec_nest ,input_map ,getsize_fits ,npix2nside & 
       ,nside2npix ,printerror ,get_healpix_main_dir ,unfold_weightsfile

  implicit none
  
  public mask_matrix ,mask_vec ,map2vec ,reorder_map ,get_pixel_vectors &
       ,read_map ,read_cls_from_file ,ext_prod ,symmetrize_matrix       &
       ,map_stats ,sky_mask ,order_flag ,stop_on_fits_error ,get_ring_weights &
       ,Pvec2IQU ,IQU2Pvec ,validate_mapfile ,get_ring_weights_pol ,vec2map

  private

  character(len=*)     ,parameter :: order_flag(2) = ['RING','NEST']
  integer(i4b) ,public ,parameter :: myTT=1 ,myEE=2 ,myBB=3 ,myTE=4 &
       ,myTB=5 ,myEB=6 ,maxlen=filenamelen
  
  type :: map_stats

     integer(i4b) :: nside ,nstokes ,ordering 
     integer(i8b) :: npix

   contains

     generic   :: set => set_stats_from_file ,set_stats
     procedure :: set_stats_from_file
     procedure :: set_stats

  end type map_stats
  
  type sky_mask

     type(map_stats)       :: s
     integer(i4b)          :: ntemp ,nq ,nu ,nqu ,ntot
     real(dp) ,allocatable :: Tvec(:,:) ,Qvec(:,:) ,Uvec(:,:) ,m(:,:)
     logical  ,allocatable :: lmask(:)
     
   contains

     generic   :: set => set_mask_from_file ,set_mask_from_map
     procedure :: set_mask_from_file
     procedure :: set_mask_from_map
     procedure :: free_mem => free_mask_mem

  end type sky_mask

  interface map2vec
     module procedure map2vec_dp ,map2vec_sp ,map2vec_lg
  end interface

  interface vec2map
     module procedure vec2map_dp
  end interface vec2map
  
  interface IQU2Pvec
     module procedure IQU2Pvec_dp ,IQU2Pvec_sp ,IQU2Pvec_lg 
  end interface IQU2Pvec

  interface Pvec2IQU
     module procedure Pvec2IQU_dp !,IQU2Pvec_sp ,IQU2Pvec_lg 
  end interface Pvec2IQU
  
  interface reorder_map
     module procedure reorder_map_dp ,reorder_map_sp ,reorder_map_lg
  end interface

  interface get_pixel_vectors
     module procedure get_pixel_vectors_dp 
  end interface

  interface read_map
     module procedure read_map_dp
  end interface

  interface ext_prod
     module procedure ext_prod_dp ,ext_prod_sp
  end interface

  interface symmetrize_matrix
     module procedure symmetrize_matrix_dp
  end interface

contains

  subroutine set_stats_from_file(self,file)

    class(map_stats) ,intent(inout) :: self
    character(len=*) ,intent(in)    :: file

    self%npix = getsize_fits(trim(file) ,nmaps=self%nstokes ,nside=self%nside &
         ,ordering=self%ordering)

  end subroutine set_stats_from_file

  subroutine set_stats(self,nside,nstokes,npix,ordering)
    class(map_stats) ,intent(inout) :: self
    integer(i4b)     ,intent(in)    :: nside ,nstokes ,ordering
    integer(i8b)     ,intent(in)    :: npix

    self%nside    = nside
    self%npix     = npix
    self%nstokes  = nstokes
    self%ordering = ordering

  end subroutine set_stats

  subroutine free_mask_mem(self)

    class(sky_mask) ,intent(inout) :: self
    
    if(allocated(self%m))    deallocate(self%m)
    if(allocated(self%Tvec)) deallocate(self%Tvec)
    if(allocated(self%Uvec)) deallocate(self%Qvec)
    if(allocated(self%Qvec)) deallocate(self%Uvec)
    if(allocated(self%lmask)) deallocate(self%lmask)

  end subroutine free_mask_mem
  
  subroutine set_mask_from_file(self,file) 
    class(sky_mask)  ,intent(inout) :: self
    character(len=*) ,intent(in)    :: file

    real(dp) ,allocatable :: tmp(:,:)
    
    associate (s => self%s)
      call s%set(trim(file))
      
      select case (s%ordering)
      case (1,2)
         write(*,*) 'mask in '//order_flag(s%ordering)//' ordering'
         write(*,*) 'make sure NCM is in '//order_flag(s%ordering)//' ordering'
      case default
         stop 'mask ordering not defined'
      end select

      call read_map(trim(file),tmp)
      call self%set_mask_from_map(tmp,s)

    end associate

  end subroutine set_mask_from_file

  subroutine set_mask_from_map(self,map,stats)

    class(sky_mask) ,intent(inout) :: self
    real(dp)        ,intent(in)    :: map(0:,:)
    type(map_stats) ,intent(in)    :: stats

    self%s = stats
    allocate(self%m, source=map)
    allocate(self%lmask(0:stats%npix*stats%nstokes-1))
    self%lmask = map2vec(map .gt. 0.5)

    associate (npix => self%s%npix)
      self%ntemp = count(self%lmask(0:npix-1))
      if(self%s%nstokes .ge. 2) then
         self%nq    = count(self%lmask(npix:2*npix-1))
         self%nu    = count(self%lmask(2*npix:3*npix-1))
      else
         self%nq = 0
         self%nu = 0
      end if
    end associate
    self%nqu  = self%nq +self%nu
    self%ntot = self%ntemp +self%nqu
    
    allocate(self%Tvec(3,self%ntemp),self%Qvec(3,self%nq),self%Uvec(3,self%nu))
    call get_pixel_vectors(reshape(self%lmask,[int(stats%npix),stats%nstokes]) ,self%s ,self%Tvec ,self%Qvec ,self%Uvec)
    
  end subroutine set_mask_from_map

  subroutine symmetrize_matrix_dp(mat,upper)
    real(dp) ,intent(inout)       :: mat(:,:)
    logical ,intent(in) ,optional :: upper

    logical      :: up = .false.
    integer(i4b) :: l ,i ,j

    if(present(upper)) up = upper

    l = size(mat(:,1))
    if(.not. up) then
!$OMP PARALLELDO DEFAULT (PRIVATE) SHARED(mat,l) 
       do i=1,l
          do j=1,i-1
             mat(j,i) = mat(i,j)
          end do
       end do
    else
!$OMP PARALLELDO DEFAULT (PRIVATE) SHARED(mat,l) 
       do i=1,l
          do j=1,i-1
             mat(i,j) = mat(j,i)  
          end do
       end do
    end if

  end subroutine symmetrize_matrix_dp

  subroutine read_cls_from_file(filein,cls,err) 

    implicit none

    character(len=*)  ,intent(in)  :: filein
    real(dp)          ,intent(out) :: cls(2:,:)
    logical ,optional ,intent(out) :: err

    character(maxlen) :: tmp
    logical           :: ok 
    integer           :: istat ,lmax ,unit ,l ,idum
    
    lmax = size(cls(:,1)) +1

    cls = 0._dp
    ok = .false.

    open(newunit = unit,file=trim(filein),status='old',action='read',&
         iostat=istat)
    
    found:if(istat .eq. 0) then
       !we found the file, let's see if it's in the correct format

       !let's see if there's an header
       read(unit,'(a)') tmp
       tmp = trim(adjustl(tmp))
       if(.not. (tmp(1:1) .eq. '#')) then
          rewind(unit) !there is no header
       end if

       !now start reading the data
       read(unit,*,iostat=istat) idum,cls(2,1:4)
       if(istat .eq. 0) then
          write(*,*) 'cls file appears to have 5+ columns'
          write(*,*) 'assumed ordering is l, TT, EE, BB, TE'
          do l=3,lmax
             read(unit,*,iostat=istat) idum,cls(l,1:4)
             if (istat .ne. 0) then
                close(unit)
                exit found
             end if
          end do
          ok = .true.
          close(unit)
       else
          rewind(unit)
          read(unit,*,iostat=istat) idum,cls(2,myTT),cls(2,myEE),cls(2,myTE)
          if(istat .eq.0) then
             write(*,*) 'cls file appears to have 4 columns'
             write(*,*) 'assumed ordering is l, TT, EE, TE'
             do l=3,lmax
                read(unit,*,iostat=istat) idum,cls(l,myTT),cls(l,myEE),cls(l,myTE)
                if (istat .ne. 0) then
                   close(unit)
                   exit found
                end  if
             end do
             ok = .true.
             close(unit)
          end if
       end if
    end if found

    if(present(err)) then
       err = .not. ok
    else if (.not. ok) then
       write(0,*) 'WARNING: '//trim(filein)//' not found or not enough columns'
       stop 'stopping'
    end if
       
  end subroutine read_cls_from_file

  subroutine mask_matrix(mask,ncvm)

    logical               ,intent(in)    :: mask(:)
    real(dp) ,allocatable ,intent(inout) :: ncvm(:,:)

    logical  ,allocatable :: mmask(:,:)
    real(dp) ,allocatable :: tmp(:)
    integer(i4b)          :: i ,j ,n ,ntrue

    n = size(mask)
    allocate(mmask(n,n))

    do i=1,n
       do j=1,n
          mmask(j,i) = mask(j) .and. mask(i)
       end do
    end do

    ntrue = count(mask)
    allocate(tmp(ntrue*ntrue))
    tmp(:) = pack(ncvm,mmask)

    deallocate(mmask,ncvm)
    allocate(mmask(ntrue,ntrue),ncvm(ntrue,ntrue))
    mmask = .true.
    ncvm = unpack(tmp(:),mask= mmask,field=0.d0)
    deallocate(tmp,mmask)

  end subroutine mask_matrix

  subroutine mask_vec(mask,v)
    logical  ,intent(in)                 :: mask(:)
    real(dp) ,allocatable ,intent(inout) :: v(:)

    real(dp) ,allocatable :: tmp(:)
    integer(i4b)          :: ntrue

    ntrue = count(mask)
    allocate(tmp(ntrue))
    tmp = pack(v,mask=mask)

    call move_alloc(from=tmp,to=v)

  end subroutine mask_vec

  subroutine Pvec2IQU_dp(v,map)
    !the 1D array contains only the polarization part of the map    
    real(dp) ,intent(in)  :: v(1:)
    real(dp) ,intent(out) :: map(0:,:)

    integer(i4b)          :: n ,nmaps
    integer(i8b)          :: npix

    npix  = size(map(:,1))
    nmaps = size(map(0,:))

    if(npix*(nmaps-1) .ne. size(v(:))) then
       map = hpx_dbadval
       return
    else
       do n = 1,nmaps-1
          map(0:npix -1,n+1) = v((n-1)*npix +1:n*npix)
       end do
    end if

  end subroutine Pvec2IQU_dp

  subroutine vec2map_dp(v,map)
    real(dp) ,intent(in)  :: v(1:)
    real(dp) ,intent(out) :: map(0:,:)

    integer(i4b)          :: n ,nmaps
    integer(i8b)          :: npix

    npix  = size(map(:,1))
    nmaps = size(map(0,:))

    if(npix*nmaps .ne. size(v(:))) then
       map = hpx_dbadval
       return
    else
       do n = 1,nmaps
          map(0:npix -1,n) = v((n-1)*npix +1:n*npix)
       end do
    end if

  end subroutine vec2map_dp
  
  function IQU2Pvec_dp(map) result(v)
    !map the polarization part of an IQU healpix map to a 1D vector
    real(dp) ,intent(in)  :: map(0:,:)
    real(dp) ,allocatable :: v(:)
    
    if (size(map(0,:)) .ne. 3) then
       !input is not an IQU healpix map object
       allocate(v(1) ,source = hpx_dbadval)
    else
       v = map2vec_dp(map(0:,2:3))
    end if
    
  end function IQU2Pvec_dp
  
  function IQU2Pvec_sp(map) result(v)
    !map the polarization part of an IQU healpix map to a 1D vector
    real(sp) ,intent(in)  :: map(0:,:)
    real(sp) ,allocatable :: v(:)
    
    if (size(map(0,:)) .ne. 3) then
       !input is not an IQU healpix map object
       allocate(v(1) ,source = hpx_sbadval)
    else
       v = map2vec_sp(map(0:,2:3))
    end if
    
  end function IQU2Pvec_sp

  function IQU2Pvec_lg(map) result(v)
    !map the polarization part of an IQU healpix map to a 1D vector
    logical ,intent(in)  :: map(0:,:)
    logical ,allocatable :: v(:)
    
    if (size(map(0,:)) .ne. 3) then
       !input is not an IQU healpix map object
       allocate(v(1) ,source = .false.)
    else
       v = map2vec_lg(map(0:,2:3))
    end if
    
  end function IQU2Pvec_lg
  
  function map2vec_dp(map) result(v)
    real(dp) ,intent(in)  :: map(0:,:)
    real(dp) ,allocatable :: v(:)

    integer(i4b)          :: i ,npix ,nmaps

    npix  = size(map(:,1))
    nmaps = size(map(0,:))

    allocate(v(npix*nmaps))
    
    do i = 1,nmaps
       v(npix*(i-1)+1:npix*i) = map(0:npix-1,i)
    end do

  end function map2vec_dp

  function map2vec_sp(map) result(v)
    real(sp) ,intent(in)  :: map(0:,:)
    real(sp) ,allocatable :: v(:)

    integer(i4b)          :: i ,npix ,nmaps

    npix  = size(map(:,1))
    nmaps = size(map(0,:))

    allocate(v(npix*nmaps))
    
    do i = 1,nmaps
       v(npix*(i-1)+1:npix*i) = map(0:npix-1,i)
    end do
    
  end function map2vec_sp

  function map2vec_lg(map) result(v)
    logical ,intent(in)  :: map(0:,:)
    logical ,allocatable :: v(:)

    integer(i4b)         :: i ,npix ,nmaps

    npix  = size(map(:,1))
    nmaps = size(map(0,:))

    allocate(v(npix*nmaps))
    
    do i = 1,nmaps
       v(npix*(i-1)+1:npix*i) = map(0:npix-1,i)
    end do

  end function map2vec_lg

  subroutine reorder_map_dp(map,from,to)

    real(dp) ,intent(inout)  :: map(0:,:)
    integer(i4b) ,intent(in) :: from ,to

    integer(i4b)             :: nside

    if(from .ne. to) then
       nside = npix2nside(size(map(:,1)))
       select case(from)
       case (1) 
          call convert_ring2nest(nside,map)
       case (2) 
          call convert_nest2ring(nside,map)
       case default
          stop 'unknown ordering in reorder_map'
       end select
    end if

  end subroutine reorder_map_dp

  subroutine reorder_map_sp(map,from,to)

    real(sp) ,intent(inout)  :: map(0:,:)
    integer(i4b) ,intent(in) :: from ,to

    integer(i4b)             :: nside

    if(from .ne. to) then
       nside = npix2nside(size(map(:,1)))
       select case(from)
       case (1) 
          call convert_ring2nest(nside,map)
       case (2) 
          call convert_nest2ring(nside,map)
       case default
          stop 'unknown ordering in reorder_map'
       end select
    end if

  end subroutine reorder_map_sp

  subroutine reorder_map_lg(map,from,to)

    logical  ,intent(inout)  :: map(0:,:)
    integer(i4b) ,intent(in) :: from ,to

    real(sp) ,allocatable    :: tmap(:,:)
    integer(i4b)             :: nside

    if(from .ne. to) then
       nside = npix2nside(size(map(:,1)))
       allocate(tmap(0:nside2npix(nside)-1,size(map(0,:))))

       where(map) 
          tmap =  1.
       elsewhere
          tmap = -1.
       end where

       call reorder_map_sp(tmap,from,to)

       where(tmap .gt. 0.)
          map = .true.
       elsewhere
          map = .false.
       end where
       deallocate(tmap)

    end if

  end subroutine reorder_map_lg

  subroutine get_pixel_vectors_dp(lmask ,stats ,Tvec ,Qvec ,Uvec)
    logical         ,intent(in)  :: lmask(0:,:)
    type(map_stats) ,intent(in)  :: stats
    real(dp)        ,intent(out) :: Tvec(:,:) ,Qvec(:,:) ,Uvec(:,:)   

    integer(i4b) :: j ,jdum 
    real(dp)     :: mod

    associate(nside => stats%nside ,order => stats%ordering ,npix => stats%npix)

    jdum = 0
    do j = 0,npix -1
       if(lmask(j,1)) then
          jdum = jdum +1
          select case(order)
          case(1)
             call pix2vec_ring(nside,j,Tvec(:,jdum))
          case(2)
             call pix2vec_nest(nside,j,Tvec(:,jdum))
          case default
             stop 'unknown ordering'
          end select

          mod = sqrt(sum(Tvec(:,jdum)**2))
          Tvec(:,jdum) = Tvec(:,jdum)/mod
       end if
    end do

    jdum = 0
    do j = 0,npix -1
       if(lmask(j,2)) then
          jdum = jdum +1
          select case(order)
          case(1)
             call pix2vec_ring(nside,j,Qvec(:,jdum))
          case(2)
             call pix2vec_nest(nside,j,Qvec(:,jdum))
          case default
             stop 'unknown ordering'
          end select

          mod = sqrt(sum(Qvec(:,jdum)**2))
          Qvec(:,jdum) = Qvec(:,jdum)/mod
       end if
    end do

    jdum = 0
    do j = 0,npix -1
       if(lmask(j,3)) then
          jdum = jdum +1
          select case(order)
          case(1)
             call pix2vec_ring(nside,j,Uvec(:,jdum))
          case(2)
             call pix2vec_nest(nside,j,Uvec(:,jdum))
          case default
             stop 'unknown ordering'
          end select

          mod = sqrt(sum(Uvec(:,jdum)**2))
          Uvec(:,jdum) = Uvec(:,jdum)/mod
       end if
    end do

    end associate

  end subroutine get_pixel_vectors_dp

  subroutine read_map_dp(file,map,stats)
    character(len=*)          ,intent(in)  :: file
    type(map_stats) ,optional ,intent(in)  :: stats
    real(dp) ,allocatable     ,intent(out) :: map(:,:)

    integer(i4b) :: nstokes ,nside ,order             
    integer(i8b) :: npix

    npix = getsize_fits(trim(file) ,nmaps=nstokes ,nside=nside ,ordering=order)
    if(present(stats)) then
       if(nside .ne. stats%nside) then
          write(*,*) trim(file)//' has wrong nside'
          stop
       end if
       if(nstokes .ne. stats%nstokes) then
          write(*,*) trim(file)//' has wrong nstokes'
          stop
       end if
    end if

    if (allocated(map)) deallocate(map)
    allocate(map(0:npix-1,nstokes))
    call input_map(trim(file),map,npix,nstokes,ignore_polcconv=.true.)
    if(present(stats)) then
       if (order .ne. stats%ordering) then
          select case(order)
          case (1)
             call convert_ring2nest(nside,map)
          case (2)
             call convert_ring2nest(nside,map)
          case default
             write(*,*) 'unknown ordering for '//trim(file)
             stop
          end select
       end if
    end if

  end subroutine read_map_dp

  pure subroutine ext_prod_dp(v1,v2,v3)
    implicit none
    real(dp),intent(in) :: v1(3),v2(3)
    real(dp),intent(out) :: v3(3)

    v3(1) = v1(2)*v2(3) - v1(3)*v2(2)
    v3(2) = v1(3)*v2(1) - v1(1)*v2(3)
    v3(3) = v1(1)*v2(2) - v1(2)*v2(1)

  end subroutine ext_prod_dp

  pure subroutine ext_prod_sp(v1,v2,v3)
    implicit none
    real(sp),intent(in) :: v1(3),v2(3)
    real(sp),intent(out) :: v3(3)

    v3(1) = v1(2)*v2(3) - v1(3)*v2(2)
    v3(2) = v1(3)*v2(1) - v1(1)*v2(3)
    v3(3) = v1(1)*v2(2) - v1(2)*v2(1)

  end subroutine ext_prod_sp

  subroutine stop_on_fits_error(err,label)
    !wrapper around healpix printerror
    integer(i4b)     ,intent(in) :: err
    character(len=*) ,intent(in) :: label

    if (err .gt. 0) then
       write(*,*) 'fitsio error on ',trim(label)
       call printerror(err)
       stop 'stopping'
    end if

  end subroutine stop_on_fits_error

  subroutine get_ring_weights(nside,weights)
    integer(i4b) ,intent(in)            :: nside
    real(dp) ,allocatable , intent(out) :: weights(:,:)

    character(len=5)   :: nstring
    character(len=256) :: datadir

    write(nstring,'(i5.5)') nside
    allocate(weights(1:2*nside,3))

    datadir  = trim(get_healpix_main_dir()) //'/data'

    call input_map(trim(datadir)//'/weight_ring_n'//nstring//'.fits' ,weights &
         ,2*nside ,3 ,fmissval=0._dp ,ignore_polcconv=.true.)

    weights = weights +1._dp

  end subroutine get_ring_weights

  subroutine get_ring_weights_pol(nside,weights)
    integer(i4b) ,intent(in)            :: nside
    real(dp) ,allocatable , intent(out) :: weights(:,:)

    character(len=5)   :: nstring
    character(len=256) :: datadir

    write(nstring,'(i5.5)') nside
    allocate(weights(1:2*nside,3))

    datadir  = trim(get_healpix_main_dir()) //"data"

    call input_map(trim(datadir)//'/weight_ring_n'//nstring//'.fits' ,weights &
         ,2*nside ,3 ,fmissval=0._dp)

    weights = weights +1._dp

  end subroutine get_ring_weights_pol
  
  subroutine get_pixel_weights(nside,weights)
    integer(i4b)          ,intent(in)  :: nside
    real(dp) ,allocatable ,intent(out) :: weights(:,:)

    integer(i8b)       :: npix
    character(len=5)   :: nstring
    character(len=256) :: dir

    write(nstring,'(i5.5)') nside

    npix = nside2npix(nside)
    allocate(weights(0:npix-1,3))

    dir  = trim(get_healpix_main_dir())//'/data'

    call unfold_weightsfile(trim(dir)//'/weight_pixel_n'//nstring//'.fits' &
         ,weights(:,1))

    weights(:,2) = weights(:,1)
    weights(:,3) = weights(:,1)

  end subroutine get_pixel_weights

  subroutine validate_mapfile(file,npix,nstokes,ordering)
    character(len=*)       ,intent(in) :: file
    integer(i4b)           ,intent(in) :: npix ,nstokes
    integer(i4b) ,optional ,intent(in) :: ordering

    integer(i4b) :: np ,ns ,ord
    logical      :: ok = .true.

    np = getsize_fits(file ,nmaps=ns ,ordering=ord)

    if (np .ne. npix) then
       write(*,*) 'file '//trim(file)//' has wrong nside'
       ok = .false.
    end if

    if (ns .ne. nstokes) then
       write(*,*) 'file '//trim(file)//' has wrong # of maps'
       ok = .false.
    end if

    if(present(ordering)) then
       if (ord .ne. ordering) then
          write(*,*) 'file '//trim(file)//' has wrong ordering'
          ok = .false.
       end if
    end if

    if(.not. ok) stop "stop in validate_file "

  end subroutine validate_mapfile


  
end module lpl_utils_mod



