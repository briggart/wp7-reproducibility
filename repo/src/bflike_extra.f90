module bflike_extra_types_mod

  use healpix_types
  implicit none

  private

  integer(i4b) ,parameter ,public :: myTT=1 ,myEE=2 ,myBB=3 ,myTE=4 ,myTB=5 ,myEB=6

  public allocatable_matrix ,fit_params ,fit_params_nl ,map_stats ,fit_data_nl

  type allocatable_vector
     real(dp) ,allocatable :: row(:)
  end type allocatable_vector
  
  type triangular_matrix

     integer :: size 
     logical :: allocated 
     type(allocatable_vector) ,allocatable :: column(:)

   contains

     procedure :: alloc => alloc_tm
     procedure :: clean => clean_tm

  end type triangular_matrix


contains


  subroutine alloc_tm(self,n)
    class(triangular_matrix) ,intent(inout) :: self
    integer ,intent(in)                     :: n
    
    integer                                 :: i
    
    if(self%allocated) call self%clean()
    
    self%size = n
    allocate(self%column(n))
    
    do i = 1,n
       allocate(self%column(i)%row(i:n))
    end do
    
    self%allocated = .true.
    
  end subroutine alloc_tm

  subroutine clean_tm(self)
    class(triangular_matrix) ,intent(inout) :: self
    integer                                 :: i
    if(.not. self%allocated) return
    
    do i = 1,self%size
       deallocate(self%column(i)%row)
    end do
    deallocate(self%column)
    
    self%size = -1
    self%allocated = .false.
    
  end subroutine clean_tm

end module bflike_extra_types_mod
