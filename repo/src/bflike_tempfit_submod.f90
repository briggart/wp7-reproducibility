submodule (bflike_smw_mod) bflike_tempfit_smod
  
  type(tempfit_data)   ,save :: D

contains

  module subroutine init_template_fit(parfile,T)

    implicit none
    character(len=*)      ,intent(in)  :: parfile
    class(tempfit_params) ,intent(out) :: T
    
    real(dp) ,allocatable :: bl(:,:) ,hw(:,:) ,clstmp(:,:)
    integer(i4b)          :: i ,j ,rcl ,unit ,l1 ,l2 ,nside
    integer(i8b)          :: isize
    character(len=80)     :: bheader(nlheader) = ''
    
!    allocate(T)
    call T%set(parfile)
    lmax    = T%lmax
    lswitch = T%lswitch
    nside   = T%nside
    ndata   = T%numdata
    
    ! read mask    
    call mask%set(T%maskfile)
    if(T%nside .ne. mask%s%nside)     stop "Mask has wrong nside"
    if(T%nstokes .ne. mask%s%nstokes) stop "Mask has wrong nstokes"
    ntemp = mask%ntemp
    nq    = mask%nq
    nu    = mask%nu
    nqu   = nq +nu
    ntot  = ntemp +nqu 
    
    !process the beam
    allocate(bl(0:4*T%nside,6),source=0._dp)
    call fits2cl(T%beamfile,bl,4*T%nside,6,bheader)
    if(T%apply_pwf) then
       allocate(hw(0:4*T%nside,3))
       call pixel_window(hw,T%nside)
       bl(:,1) = bl(:,1)*hw(:,1)
       bl(:,2) = bl(:,2)*hw(:,2)
       bl(:,3) = bl(:,3)*hw(:,3)
       bl(:,4) = bl(:,4)*sqrt(hw(:,1)*hw(:,2))
       bl(:,5) = bl(:,5)*sqrt(hw(:,1)*hw(:,3))
       bl(:,6) = bl(:,6)*sqrt(hw(:,2)*hw(:,3))
       deallocate(hw)
    end if
    allocate(cls(2:lmax,6) ,source = 0._dp)
    clnorm = cls
    call compute_clnorm(bl,clnorm)
    deallocate(bl)
    
    allocate(clstmp(2:lmax,6),source = 0._dp)
    call read_cls_from_file(trim(T%clfiducial),clstmp)

    !temp storage used in code initialization                              
    allocate(pl(1:lmax)) ;allocate(plm ,mold = pl)
    allocate(f1(2:lmax)) ;allocate(f2  ,mold = f1)
    allocate(sin1(ntot,ntot)) ;allocate(sin2 ,cos1 ,cos2 ,mold = sin1)
    TVec = mask%Tvec
    QVec = mask%QVec
    UVec = mask%Uvec
    
    !precompute the rotation angles                                    
    call precompute_rotation_angle(sin1,cos1,sin2,cos2)
    allocate(S(ntot,ntot)    ,source = 0._dp)

    l1 = 2 ;    l2 = T%lmax
    call update_ncvm(clstmp,l1,l2,S,project_mondip=T%project_mondip)

    deallocate(Tvec,Uvec,Qvec,sin1,cos1,sin2,cos2)
    deallocate(pl,plm,f1,f2,clnorm,clstmp)
    call move_alloc(from=S, to = D%S)
 
    !read ncvm
    isize = mask%s%npix*mask%s%nstokes
    allocate(NCVM(isize,isize))
    inquire(iolength=rcl) NCVM(:,1)
    open(newunit=unit,file=trim(T%ncvmfile),status='old',action='read',form='unformatted',access='direct',recl=rcl)
    do i = 1,isize
       read(unit,rec=i) NCVM(:,i)
    end do
    close(unit)

    call mask_matrix(mask%lmask,NCVM)
    NCVM = NCVM*T%ncvm_cf
    call move_alloc(from=NCVM,to=D%NCVM)
        
    allocate(D%ncvmt(T%numtemp))

    do i = 1,T%numtemp
       if(T%has_noise(i) .gt. 0.5_dp) then 
          allocate(NCVM(isize,isize))
          inquire(iolength=rcl) NCVM(:,1)
          open(newunit=unit,file=trim(T%ncvm_temp(i)),status='old',&
               action='read',form='unformatted',access='direct',recl=rcl)
          do j = 1,isize
             read(unit,rec=j) NCVM(:,j)
          end do
          close(unit)
          call mask_matrix(mask%lmask,NCVM)
          NCVM = NCVM*T%tncvm_cf(i)
          call move_alloc(to=D%ncvmt(i)%m ,from=NCVM)
       end if
    end do
    
    !read_data
    allocate(D%dt(ntot,ndata))
    call read_masked_data(T%mapfile,T%map_cf,mask,D%dt)

    !read_templates
    allocate(D%temps(ntot,ndata,T%numtemp))
    call read_masked_templates(T%templates,T%temp_cf,T%multi_temp,mask,D%temps)
    
    call mask%free_mem()
    allocate(D%tcov ,mold = D%ncvm)

  end subroutine init_template_fit

! ---------------------------------------------------------------------------

  module subroutine get_template_coeff(T,coeff,cov,chi2)

    implicit none
    class(tempfit_params) ,intent(in)  :: T
    real(dp) ,intent(out) ,allocatable :: coeff(:,:) ,cov(:,:) ,chi2(:,:)

    integer(i4b)          :: i ,j ,myunit    
    integer(i4b)          :: recl ,ndata
    integer(i8b)          :: off ,totstep
    real(dp)              :: snorm
    real(dp) ,allocatable :: clstmp(:,:) ,NCVM(:,:) ,bl(:,:) ,hw(:,:) &
         ,tmpmat(:,:) ,ncvmt(:,:,:) ,auxmat(:,:) ,templates(:,:,:) &
         ,tcov(:,:) ,tchi2(:) ,tcoeff(:)

    logical ,allocatable  :: changed(:)
    logical               :: do_update
   
    integer(i4b) ,allocatable   :: id(:) ,oldid(:)

    if(allocated(cov))   deallocate (cov)
    if(allocated(coeff)) deallocate (coeff)
    if(allocated(chi2))  deallocate (chi2)

    ndata = T%numdata
    lmax  = T%lmax

    totstep = product(T%nstep(:))

    allocate(chi2(ndata,totstep), source = 1.d100)
    allocate(tchi2(ndata))
    allocate(coeff(T%numtemp,ndata),tcoeff(T%numtemp))
    allocate(cov(T%numtemp,T%numtemp),source=0.d0)

    allocate(id(T%numtemp) ,source = 0)
    allocate(changed(T%numtemp) ,source = .false.)
    
    tcoeff = T%vmin 
    snorm = 1._dp - sum(T%has_cmb*tcoeff)
    call decompose_eff_matrix(tcoeff,D,snorm,T%has_noise)
    call compute_chi2_chol(tcoeff,D,snorm,chi2(:,1),T%loglike)
    do concurrent (j=1:T%numdata)
       coeff(:,j) = tcoeff(:)
    end do
    tchi2(:) = chi2(:,1)

    do i = 2,totstep
       call increase_index(id,T%nstep,changed)
       tcoeff = T%vmin +T%step*id
       do_update = .false.
       do j = 1,T%numtemp
          if(changed(j)) then
             if (T%has_noise(j) .gt. 0.5_dp .or. T%has_cmb(j) .gt. 0.5_dp) &
                  do_update = .true.
          end if
       end do

       snorm = 1._dp - sum(T%has_cmb*tcoeff)
       if(do_update) call decompose_eff_matrix(tcoeff,D,snorm,T%has_noise)
!       print*,T%loglike
!       print*,size(chi2(:,1)),size(chi2(1,:))
!       print*,size(D%S(1,:)),size(D%S(:,1))
!       print*,size(D%NCVM(1,:)),size(D%NCVM(:,1))
!       print*,size(D%dt(1,:)),size(D%dt(:,1))
!       stop
       call compute_chi2_chol(tcoeff,D,snorm,chi2(:,i),T%loglike)

       do concurrent (j=1:T%numdata)
          if(chi2(j,i) .le. tchi2(j)) then
             tchi2(j) = chi2(j,i)
             coeff(:,j) = tcoeff(:)
          end if
       end do
    end do

  contains

    subroutine increase_index(id,nmax,changed)

      implicit none
      integer(i4b) ,intent(inout) :: id(:)
      integer(i4b) ,intent(in)    :: nmax(:)
      logical      ,intent(out)   :: changed(:)

      integer(i4b) :: i ,nn

      nn = size(id)
      changed = .false.

      do i = nn,1,-1
         id(i) = id(i) +1
         changed(i)  = .true.
         if(id(i) .ge. nmax(i) ) then
            id(i) = 0
         else
            return
         end if
      end do
      
    end subroutine increase_index
    
  end subroutine get_template_coeff

! ---------------------------------------------------------------------------

!  subroutine read_template_fit_mcmc(parfile,p)
    
!    use healpix_modules 
!    use bflike_types_mod ,only : fit_params_nl 
    
!    implicit none
!    character(len=*)    ,intent(in)  :: parfile
!    type(fit_params_nl) ,intent(out) :: p
    
!    character(filenamelen)  :: filename 
!    character(filenamelen) ,allocatable :: templates(:)
!    integer(i4b)            :: ntemp ,lmax ,unit ,i ,nstep ,ndata ,neff

!    real(dp) ,allocatable   :: temp_cf(:) &
!         ,cmin(:)
!    real(dp)                :: ncvm_cf ,map_cf
!    type(paramfile_handle)  :: handle
!    logical                 :: project_mondip ,multi ,loglike
!    character(len=2)        :: tag
    
!    handle =  parse_init(parfile)
!    ntemp = parse_int(handle,'num_templates')
!    lmax  = parse_int(handle,'lmax')
!    ndata = parse_int(handle,'ndata')
!    call p%set(ntemplates = ntemp ,lmax = lmax ,ndata = ndata)
    
!    multi   = parse_lgt(handle,'multi',.false.)
!    project_mondip = parse_lgt(handle,'project_mondip',.true.)
!    call p%set(multi_template=multi ,project_mondip=project_mondip) 
    
!    loglike = parse_lgt(handle,'loglike',.false.)
!    call p%set(loglike=loglike) 
    
!    ncvm_cf = parse_double(handle,'ncvm_conversion_factor')
!    map_cf  = parse_double(handle,'map_conversion_factor')
!    allocate(temp_cf(ntemp))
!    do i = 1,ntemp
!       write(tag,'(i2.2)') i
!       temp_cf(i) = parse_double(handle,'template_cf_'//tag)
!    end do
!    call p%set(ncvm_cf =ncvm_cf ,data_cf =map_cf ,template_cf =temp_cf)
    
!    do i = 1,ntemp
!       write(tag,'(i2.2)') i
!       temp_cf(i) = parse_double(handle,'has_cmb_'//tag)
!    end do
!    call p%set(has_cmb = temp_cf)
    
!    do i = 1,ntemp
!       write(tag,'(i2.2)') i
!       temp_cf(i) = parse_double(handle,'has_noise_'//tag)
!    end do
!    call p%set(has_noise = temp_cf)
    
!    allocate(cmin(ntemp))
!    do i = 1,ntemp
!       write(tag,'(i2.2)') i
!       cmin(i)    = parse_double(handle,'cmin_'//tag)
!       temp_cf(i) = parse_double(handle,'cmax_'//tag)
!    end do
    
!    nstep = parse_int(handle,'nstep')
!    temp_cf = (temp_cf -cmin)/nstep
!    call p%set(cmin = cmin ,cstep = temp_cf ,nstep = nstep)
    
!    filename = parse_string(handle,'mask')
!    call p%set(maskfile=filename)
    
!    filename = parse_string(handle,'map')
!    if(ndata == 1) then
!       call p%set(datafile=[filename])
!    else
!       allocate(templates(ndata))
!       open(newunit=unit,file=filename,status='old',action='read')
!       do i=1,ndata
!          read(unit,'(a)') templates(i)
!       end do
!       close(unit)
!       call p%set(datafile=templates)
!       deallocate(templates)
!    end if
    
!    filename = parse_string(handle,'ncvm')
!    call p%set(ncvmfile=filename)
    
!    if(multi) then
!       neff = ndata*ntemp
!    else
!       neff = ntemp
!    end if
!    allocate(templates(neff))
!    filename = parse_string(handle,'template_list')
!    open(newunit=unit,file=filename,status='old',action='read')
!    do i=1,neff
!       read(unit,'(a)') templates(i)
!    end do
!    close(unit)
!    call p%set(templates=templates)
!    deallocate(templates)
    
!    allocate(templates(ntemp))
!    do i=1,ntemp
!       write(tag,'(i2.2)') i     
!       templates(i) = parse_string(handle,'template_ncvm_'//tag,'')
!       temp_cf(i)   = parse_double(handle,'template_ncvm_cf_'//tag)
!    end do
!    close(unit)
!    call p%set(ncvm_templ=templates ,ncvm_templ_cf=temp_cf)
!    deallocate(templates)
    
!    filename = parse_string(handle,'beamfile')
!    call p%set(beamfile=filename)
    
!    filename = parse_string(handle,'clfile')
!    call p%set(clfile=filename)
    
!    filename = parse_string(handle,'output_root')
!    call parse_finish(handle)
    
!  end subroutine read_template_fit_mcmc

! ---------------------------------------------------------------------------


!  subroutine init_template_fit_mcmc(P,D)

!    implicit none

!    type(fit_params_nl) ,intent(in)    :: P
!    type(fit_data_nl)   ,intent(out)   :: D

!    integer(i4b)          :: i ,j ,myunit    
!    integer(i4b)          :: recl ,info ,ndata
!    integer(i8b)          :: npixtot 
!    real(dp) ,allocatable :: clstmp(:,:) ,bl(:,:) ,hw(:,:) &
!         ,tmpmat(:,:) ,auxmat(:,:) 

!    logical ,allocatable  :: lmask(:,:) ,vmask(:) ,mmask(:,:) 
   
!    character(len=80)           :: bheader(nlheader) = ''

!    type(map_stats) :: mstats

!    ndata = P%ndata
    
!    lmax = P%lmax

!    mstats%npix = getsize_fits(trim(P%maskfile) ,nmaps=mstats%nstokes &
!         ,nside=mstats%nside ,ordering=mstats%ordering)

!    npixtot = mstats%npix*mstats%nstokes

!    select case (mstats%ordering)
!    case (1,2)
!       write(*,*) 'mask in '//order_flag(mstats%ordering)//' ordering'
!       write(*,*) 'make sure NCM is in '//order_flag(mstats%ordering)//' ordering'
!    case default
!       stop 'mask ordering not defined'
!    end select

!    allocate(tmpmat(0:mstats%npix-1 ,mstats%nstokes))
!    call input_map(trim(P%maskfile) ,tmpmat ,mstats%npix ,mstats%nstokes)
    
!    allocate(lmask(0:mstats%npix-1 ,mstats%nstokes))
!    lmask = tmpmat .gt. 0.5
!    deallocate(tmpmat)

!    ntemp = count(lmask(:,1))
!    if(mstats%nstokes .ge. 2) then
!       nq = count(lmask(:,2))
!       nu = count(lmask(:,3))
!    else
!       nq = 0
!       nu = 0
!    end if
!    nqu  = nq +nu
!    ntot = ntemp +nqu
!    write(*,*) ntemp,nq,nu

!    allocate(Tvec(3,ntemp),Qvec(3,nq),Uvec(3,nu))

!    call get_pixel_vectors(lmask,mstats,Tvec,Qvec,Uvec)

!beam
!    allocate(bl(0:4*mstats%nside,6))
!    call fits2cl(trim(P%beamfile),bl,4*mstats%nside,6,bheader)

!    allocate(hw(0:4*mstats%nside,3))
!    call pixel_window(hw,mstats%nside)

!    bl(:,1) = bl(:,1)*hw(:,1)
!    bl(:,2) = bl(:,2)*hw(:,2)
!    bl(:,3) = bl(:,3)*hw(:,3)
!    bl(:,4) = bl(:,4)*sqrt(hw(:,1)*hw(:,2))
!    bl(:,5) = bl(:,5)*sqrt(hw(:,1)*hw(:,3))
!    bl(:,6) = bl(:,6)*sqrt(hw(:,2)*hw(:,3))

!    allocate(clnorm(2:lmax,6))

!    call compute_clnorm(bl,clnorm)
!    deallocate(hw ,bl)

!compute signal matrix 
!    allocate(D%S(ntot,ntot))
!    D%S = 0._dp

!    allocate(cls(2:lmax,6))
!    allocate(pl(1:lmax),plm(1:lmax),f1(2:lmax),f2(2:lmax))

!precompute the rotation angles
!    allocate(sin1(ntot,ntot),cos1(ntot,ntot),sin2(ntot,ntot),cos2(ntot,ntot))
!    call precompute_rotation_angle(sin1,cos1,sin2,cos2)

!    allocate(clstmp(2:lmax,6))
!    call read_cls_from_file(trim(P%clfile),clstmp)

!    call update_ncvm(clstmp,2,lmax,D%S,project_mondip=P%project_mondip)
!    deallocate(clstmp,f1,f2,pl,plm,cls,sin1,cos1,sin2,cos2)

!ncvm
!    allocate(D%ncvm(ntot,ntot))
!    allocate(D%ncvmt(P%ntemplates))

!    allocate(auxmat(npixtot,npixtot))
!    inquire(iolength=recl) auxmat

!    open(newunit=myunit,file=trim(P%ncvmfile),status='old',action='read', &
!         form='unformatted',access='direct',recl=recl)
!    read(myunit ,rec=1) auxmat
!    close(myunit)

!    auxmat = auxmat*P%ncvm_cf

!    allocate(tmpmat(ntot*ntot,1))
!    allocate(vmask(npixtot))
!    vmask = pack(lmask,mask=.true.)

!    allocate(mmask(npixtot,npixtot))

!    do i=1,npixtot
!       do j=1,npixtot
!          mmask(j,i) = vmask(j) .and. vmask(i)
!       end do
!    end do

!    deallocate(vmask)
!    tmpmat(:,1) = pack(auxmat,mmask)
!    D%ncvm = reshape(tmpmat(:,1),[ntot,ntot])

!    do i = 1,P%ntemplates
!       if(P%has_noise(i) .gt. 0.5) then
!          open(newunit=myunit,file=trim(P%ncvm_templ(i)),status='old',action='read', &
!               form='unformatted',access='direct',recl=recl)
!          read(myunit,rec=1) auxmat
!          close(myunit)
!          auxmat = auxmat*P%ncvm_templ_cf(i)
          
!          tmpmat(:,1) = pack(auxmat,mmask)
!          call D%ncvmt(i)%alloc(ntot,ntot)
!          D%ncvmt(i)%m = reshape(tmpmat(:,1),[ntot,ntot])
!       end if
!    end do
!    deallocate(tmpmat)
!    deallocate(mmask)

!read_data
!    allocate(D%dt(ntot,ndata),stat=info)
!    call read_masked_data(P%datafile,P%data_cf,lmask,mstats,D%dt)

!read_templates
!    allocate(D%templates(ntot,ndata,P%ntemplates))
!    call read_masked_templates(P%templates,P%temp_cf,P%multi_templ,lmask,mstats,D%templates)

!    allocate(D%tcov(ntot,ntot))

!  end subroutine init_template_fit_mcmc

! ---------------------------------------------------------------------------
  
  module subroutine get_template_loglike_mcmc(P,D,coeff,lglike)

    implicit none
    type(fit_params_nl) ,intent(in)    :: P
    type(fit_data_nl)   ,intent(inout) :: D
    real(dp)            ,intent(in)    :: coeff(:)
    real(dp)            ,intent(out)   :: lglike

    real(dp)                           :: snorm ,xlike(1)
    
    snorm = 1._dp - sum(P%has_cmb*coeff(1:P%ntemplates))
    call decompose_eff_matrix(coeff(1:P%ntemplates),D,snorm,P%has_noise)
    call compute_chi2_chol(coeff(1:P%ntemplates),D,snorm,xlike,loglike=.false.)
    lglike = xlike(1)
    
  end subroutine get_template_loglike_mcmc

  subroutine compute_chi2_chol(coeff,D,snorm,chi2,loglike)

    implicit none    
    real(dp)            ,intent(in) :: coeff(:) ,snorm 
    class(tempfit_data) ,intent(in) :: D
    real(dp)           ,intent(out) :: chi2(:)
    logical   ,intent(in) ,optional :: loglike 

    real(dp) ,allocatable :: tdt(:,:) ,auxdt(:,:) 
    integer(i4b)          :: ntemp ,ntot ,i ,info ,ndata ,j
    real(dp)              :: lgdet 

    ntemp = size(coeff)
    ntot  = size(D%dt(:,1))
    ndata = size(D%dt(1,:))

    allocate(tdt  ,source = D%dt)

    do i =1,ntemp
       do concurrent(j=1:ndata)
          tdt(:,j)   = tdt(:,j)  -D%temps(:,j,i)*coeff(i)
       end do
    end do

    allocate(auxdt ,source = tdt)

    call dpotrs( 'L', ntot, ndata, D%tcov, ntot, tdt, ntot, info )
    
    lgdet = 0.d0

    if(present(loglike)) then
       if(loglike) then
          do i = 1,ntot
             lgdet = lgdet +log(D%tcov(i,i))
          end do
          lgdet = 2.d0*lgdet !-2*ntot*log(snorm) 
       end if
    end if

    do concurrent(i = 1:ndata)
       chi2(i) = sum(auxdt(:,i)*tdt(:,i))
    end do

    chi2 = chi2 +lgdet

  end subroutine compute_chi2_chol

! ---------------------------------------------------------------------------

  subroutine decompose_eff_matrix(coeff,D,snorm,has_noise)
    implicit none
    real(dp)               ,intent(in) :: coeff(:) ,snorm
    class(tempfit_data) ,intent(inout) :: D
    real(dp)     ,intent(in) ,optional :: has_noise(:)

    real(dp) ,allocatable :: q(:)
    integer(i4b)          :: ntemp ,ntot ,i ,info ,ndata 

    ntemp = size(coeff)
    ntot  = size(D%dt(:,1))

    allocate(q(ntemp))
    if (present(has_noise)) then
       q = has_noise
    else
       q = 1._dp
    end if

    D%tcov  = D%ncvm +snorm**2*D%S

    do i =1,ntemp
       if(q(i) .gt. 0.5) D%tcov = D%tcov +D%ncvmt(i)%m(:,:)*coeff(i)**2
    end do
    
    call dpotrf('L', ntot, D%tcov, ntot, info)
    if(info .ne. 0) then
       write(*,*) 'tcov is not invertible'
       write(*,*) 'info = ',info
       stop
    end if

  end subroutine decompose_eff_matrix

! ---------------------------------------------------------------------------

  subroutine read_masked_data(file,cf,mask,dt)
    implicit none
    character(len=*) ,intent(in) :: file
    real(dp)         ,intent(in) :: cf
    class(sky_mask)  ,intent(in) :: mask
    real(dp)        ,intent(out) :: dt(:,:)
    
    real(dp) ,allocatable :: map(:,:)
    integer(i4b)          :: i ,ndata ,unit
    character(maxlen)     :: ifile
    
    ndata = size(dt(1,:))

    if (ndata .eq. 1) then
       write(*,*) 'reading data ' ,i ,trim(file)
       call read_map(file,map,mask%s)
       map = map*cf
       dt(:,1) = pack(reshape(map,shape=shape(mask%lmask)),mask%lmask)
    else
       open(newunit=unit,file=trim(file),status='old',action='read')
       do i = 1,ndata
          read(unit,'(a)') ifile
          write(*,*) 'reading data ' ,i ,trim(ifile)
          call read_map(trim(ifile),map,mask%s)
          map = map*cf
          dt(:,i) = pack(reshape(map,shape=shape(mask%lmask)),mask%lmask)
       end do
       close(unit)
    end if
  end subroutine read_masked_data

! ---------------------------------------------------------------------------

  subroutine read_masked_templates(file,cf,multi,mask,t)
    implicit none
    character(len=*) ,intent(in) :: file(:)
    real(dp)         ,intent(in) :: cf(:)
    logical          ,intent(in) :: multi
    class(sky_mask)  ,intent(in) :: mask   
    real(dp)        ,intent(out) :: t(:,:,:)
    
    real(dp) ,allocatable :: map(:,:)
    integer(i4b)          :: i ,j ,idum ,ndata ,ntemp ,neff 

    ntemp = size(t(1,1,:))
    ndata = size(t(1,:,1))

    if(multi) then 
       neff = ndata
    else
       neff = 1 !use the same templates for all the data maps
    end if

    idum = 0
    do j = 1,ntemp
       do i = 1,neff
          idum = idum +1
          write(*,*) 'reading template ',i,j,trim(file(idum))
          call read_map(file(idum),map,mask%s)
          map = map*cf(j)
          t(:,i,j) = pack(reshape(map,shape=shape(mask%lmask)),mask%lmask)
       end do
    end do
    
    if(.not. multi) then
       do j = 1,ntemp 
          do i = 2,ndata
             t(:,i,j) = t(:,1,j)
          end do
       end do
    end if

  end subroutine read_masked_templates

end submodule bflike_tempfit_smod



