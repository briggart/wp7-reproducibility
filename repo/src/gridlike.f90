program gridlike

  use debugging_mod
  use lpl_utils_mod
  use bflike_types_mod
  use bflike_smw_mod
  use healpix_types
  use paramfile_io
  use healpix_modules ,only : add_card ,write_bintabh ,write_minimal_header
  implicit none

  integer(i4b),parameter :: nlheader = 256
  integer(i4b)           :: i ,j ,l ,maxl ,switchl ,numdata ,ncmd ,lent &
       ,npt ,myunit ,clunit ,nwrite

  real(dp)               :: maxlike
  real(dp),allocatable   :: clsref(:,:) ,cls(:,:) ,like(:,:) ,aexp(:,:) &
       ,alog(:) ,tmpvec(:,:)
  real(sp)               :: t2,t1
  character(filenamelen) :: filename, root ,fidfile
  character(len=80)      :: header(nlheader) = ''
  logical                :: project_mondip
  type(paramfile_handle) :: handle

  ncmd = command_argument_count()
  if(ncmd .ne. 1) then
     write(*,*) 'CRITICAL ERROR : usage gridlike [params.ini]'
     stop
  else
     call get_command_argument(1,filename)
  end if

  call init_pix_like(trim(filename),maxl,switchl,numdata)

  handle = parse_init(filename)
  npt = parse_int(handle,'npt')

  root = parse_string(handle,'models')
  open(newunit=myunit,file = trim(root) ,status='old',action='read')

  root = parse_string(handle,'output_root')
  lent = len_trim(root)
  if(lent .gt. 0) then
     if (.not. root(lent:lent) .eq. '_') then
        root(lent+1:lent+1) = '_'
     end if
  end if

  print*,numdata,maxl

  call add_card(header,'CODE','slice_general_smw')
  call add_card(header,'NSIMS',numdata)
  call add_card(header,'NPTS',npt)
  call add_card(header,'LMAX',maxl)
  call add_card(header,'LSWITCH',switchl)
  allocate(like(npt,numdata))
  allocate(aexp(npt,numdata),alog(numdata))
  allocate(clsref(2:maxl,6),cls(2:maxl,6))

  aexp = 0.d0
  alog = 0.d0
  clsref = 0.d0
  cls  = 0.d0

  fidfile = parse_string(handle,'clfiducial')
  write(*,*) 'reading fiducial cls from = ',trim(fidfile)
  call add_card(header,'FID_CLS',trim(fidfile))

  open(newunit=clunit,file=trim(fidfile),status='old',action='read')
  do i=2,maxl
     read(clunit,*) l,clsref(l,myTT),clsref(l,myEE),clsref(l,myBB),clsref(l,myTE)
  end do
  close(clunit)

!  call read_key(2001,trim(filename),'mapfile',val_char=fidfile)
!  call add_card(header,'MAPFILE',trim(fidfile))
  
  fidfile = parse_string(handle,'datafile')
  call add_card(header,'datafile',trim(fidfile))

  project_mondip = parse_lgt(handle,'project_mondip')
  call add_card(header,'PROJECT_MD',project_mondip)

  call parse_summarize(handle)
  call parse_check_unused(handle)
  call parse_finish(handle)

  cls = clsref!*1.d-12

  do j=1,npt

     read(myunit,'(a)') filename

     open(newunit=clunit,file=trim(filename),status='old',action='read')
     do i=2,switchl
        read(clunit,*) l,cls(l,myTT),cls(l,myEE),cls(l,myBB),cls(l,myTE)
     end do
     close(clunit)

     call cpu_time(t1)
     call get_pix_loglike(cls,like(j,1:numdata),aexp(j,1:numdata),alog(1:numdata))
     call cpu_time(t2)
     write(*,*) j,' out of',npt, like(j,1),' in ',t2-t1,'sec'  

     if(debugging) then
        write(*,*) 'done'
        stop
     end if

  end do
  close(myunit)

  allocate(tmpvec(npt,2*numdata))
  nwrite = npt*(2*numdata)
  do i = 1,numdata
     maxlike = -1.d100
     tmpvec(:,2*i-1) = like(:,i) 
     tmpvec(:,2*i+0) = -aexp(:,i)
  end do

  deallocate(like)
  allocate(like(0:nwrite-1,1))
  like(:,1) = pack(tmpvec,mask=.true.)
  deallocate(tmpvec)
  print*,nwrite

  call write_bintabh(like, int(npt*(2*numdata),i8b), 1, header, nlheader, &
       '!'//trim(root)//'slice.fits',repeat=1,firstpix=0_i8b)
  deallocate(like)

end program gridlike
