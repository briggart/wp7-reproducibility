program tempfit

  use debugging_mod
  use healpix_types
  use paramfile_io
  use lpl_utils_mod    ,only : maxlen
  use bflike_types_mod ,only : tempfit_params ,allocatable_matrix
  use bflike_smw_mod   ,only : init_template_fit ,get_template_coeff 

  implicit none

  integer(i4b)             ,allocatable :: indx(:)
  real(dp)                 ,allocatable :: coeff(:,:) ,cov(:,:) ,chi2(:,:) &
       ,tmpchi(:,:) ,tcov(:,:,:) ,avrg(:,:) ,stdv(:,:)
  type(allocatable_matrix) ,allocatable :: marg(:) ,cross(:,:,:) ,xpar(:)
  logical                  ,allocatable :: changed(:)

  character(maxlen)      :: parfile ,oroot 
  integer(i4b)           :: ncmd ,unit ,j ,i ,ndata ,n ,k 
  integer(i8b)           :: totstep 

  real(dp)               :: best ,st ,wgt
  type(tempfit_params)   :: P

  type(paramfile_handle) :: handle
  character(len=2)       :: tag
  character(len=4)       :: xtag

  ncmd = command_argument_count()
  if(ncmd .ne. 1) then
     write(*,*) 'CRITICAL ERROR : usage fit_template params.ini'
     stop
  else
     call get_command_argument(1,parfile)
  end if

  call init_template_fit(parfile,P)
  
  handle = parse_init(parfile)
  oroot  = parse_string(handle,'output_root')
  call parse_finish(handle)
  
  call get_template_coeff(P,coeff,cov,chi2)
  write(0,*) 'done' 
  
  totstep = product(P%nstep(:))

  allocate(tmpchi ,mold = chi2)
  
  do j = 1,P%numdata
     best = minval(chi2(j,:))
     tmpchi(j,:) = exp(-.5d0*(chi2(j,:) -best))
  end do

  allocate(marg(P%numtemp))
  do i = 1,P%numtemp
     call marg(i)%alloc(nrow=P%nstep(i),ncol=P%numdata)
  end do
  allocate(cross(P%numdata,P%numtemp,P%numtemp))
  do i = 1,P%numtemp
     do j = i,P%numtemp
        do k = 1,P%numdata 
           call cross(k,j,i)%alloc(nrow=P%nstep(j),ncol=P%nstep(i))
        end do
     end do
  end do

  allocate(indx(P%numtemp)    ,source = 0)
  allocate(changed(P%numtemp) ,source = .false.)
  
  do i  = 1,totstep
     do j = 1,P%numtemp
        wgt = 1._dp
        do k = 1,P%numtemp
           if(k.ne.j) then
              if(indx(k) .eq. 0 .or. indx(k) .eq. (P%nstep(k)-1)) wgt = wgt/2 
           end if
        end do
        marg(j)%m(indx(j)+1,:) = marg(j)%m(indx(j)+1,:) + wgt*tmpchi(:,i) 
     end do
     do j = 1,P%numtemp
        do n = j,P%numtemp
           wgt = 1._dp
           do k = 1,P%numtemp
              if(k.ne.j .and. k .ne.n) then
                 if(indx(k) .eq. 0 .or. indx(k) .eq. (P%nstep(k)-1)) &
                      wgt = wgt/2 
              end if
           end do

           do k = 1,P%numdata
              cross(k,n,j)%m(indx(n)+1,indx(j)+1) = &
                   cross(k,n,j)%m(indx(n)+1,indx(j)+1) +wgt*tmpchi(k,i)
           end do
        end do
     end do
     call increase_indx(indx,P%nstep,changed)
  end do

  do j=1,P%numdata
     do i =1,P%numtemp
        !reweight first and last point for trapezoidal integration 
        marg(i)%m(1,j)          = marg(i)%m(1,j)/2 
        marg(i)%m(P%nstep(i),j) = marg(i)%m(P%nstep(i),j)/2 
        st = sum(marg(i)%m(:,j)) 
        marg(i)%m(:,j) = marg(i)%m(:,j)/st
        do n = i,P%numtemp
           st = sum(cross(j,n,i)%m)
           cross(j,n,i)%m = cross(j,n,i)%m/st
        end do
     end do
  end do

  allocate(xpar(P%numtemp),avrg(P%numtemp,P%numdata),stdv(P%numtemp,P%numdata))
  
  do i =1,P%numtemp
     call xpar(i)%alloc(nrow=P%nstep(i),ncol=1)
     do j = 1,P%nstep(i)
        xpar(i)%m(j,1) = P%vmin(i) +(j-1)*P%step(i)
     end do
     do j = 1,P%numdata
        avrg(i,j) = sum(xpar(i)%m(:,1)*marg(i)%m(:,j)) 
        stdv(i,j) = sqrt(sum(marg(i)%m(:,j)*(xpar(i)%m(:,1) -avrg(i,j))**2))
     end do
  end do

  allocate(tcov(P%numtemp,P%numtemp,P%numdata) ,source = 0._dp)
  do j = 1,P%numdata
     do i = 1,P%numtemp
        do n = 1,P%numtemp
           do k = 1,P%nstep(i)
              tcov(n,i,j) = tcov(n,i,j) +sum(cross(j,n,i)%m(:,k)*&
                   (xpar(n)%m(:,1)-avrg(n,j))*(xpar(i)%m(k,1)-avrg(i,j)))
           end do
        end do
     end do
  end do

  do j = 1,P%numdata
     print*,':::',j
     write(xtag,'(i4.4)') j-1
     open(newunit=unit,file=trim(oroot)//'_'//xtag//'_fit_coeff.txt',status='unknown',action='write')
     best = minval(chi2(j,:))
     do i=1,P%numtemp
        write(unit,'(3(2x,f15.10),2x,f17.10)') coeff(i,j),avrg(i,j),stdv(i,j),best
     end do
     close(unit)
     open(newunit=unit,file=trim(oroot)//'_'//xtag//'_fit_cov.txt',status='unknown',action='write')
     do i=1,P%numtemp
        do n=i,P%numtemp
           write(unit,'(i3,2x,i3,2x,e18.10)') n,i,tcov(n,i,j)
        end do
     end do
     close(unit)
  end do

contains

  subroutine increase_indx(id,nmax,changed)

    implicit none
    integer(i4b) ,intent(inout) :: id(:)
    integer(i4b) ,intent(in)    :: nmax(:)
    logical      ,intent(out)   :: changed(:)
    
    integer(i4b) :: i ,nn
    
    nn = size(id)
    changed = .false.
    do i = nn,1,-1
       id(i) = id(i) +1
       changed(i)  = .true.
       if(id(i) .ge. nmax(i)) then
          id(i) = 0
       else
          return
       end if
    end do
    
  end subroutine increase_indx

end program tempfit
