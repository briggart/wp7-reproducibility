program qml

  use debugging_mod
  use bflike_types_mod
  use bflike_smw_mod
  use healpix_types ,only : dp ,sp ,i4b ,filenamelen
  use paramfile_io
  use healpix_modules , only : add_card, write_asctab, write_minimal_header 

  implicit none

  integer(i4b),parameter :: nlheader = 256
  integer(i4b)           :: i ,maxl ,switchl ,numdata ,ncmd ,lent

  real(dp),allocatable   :: clsout(:,:,:) ,clserr(:,:)
  real(sp)               :: t2,t1
  character(filenamelen) :: filename, root ,fidfile
  character(len=5)       :: istring
  character(len=80)      :: header(nlheader)
  logical                :: do_qml ,decouple_tp
  type(paramfile_handle) :: handle

  ncmd = command_argument_count()

  if(ncmd .ne. 1) then
     write(*,*) 'CRITICAL ERROR : usage qml [params.ini]'
     stop
  else
     call get_command_argument(1,filename)
  end if

  call init_pix_like(trim(filename),maxl,switchl,numdata)

  handle = parse_init(trim(filename))
  root   = parse_string(handle,'output_root') 
  do_qml = parse_lgt(handle,'do_qml')
  decouple_tp = parse_lgt(handle,'decouple_tp')

  lent = len_trim(root)
  if(lent .gt. 0) then
     if (.not. root(lent:lent) .eq. '_') then
        root(lent+1:lent+1) = '_'
     end if
     if(.not. root(1:1) .eq. '!') then
        do i=lent+2,2,-1
           root(i:i) = root(i-1:i-1)
        end do
        root(1:1) = '!'
     end if
  end if

  print*,numdata,maxl
  

  header = ''

  call write_minimal_header(header,'CL',nlmax=switchl,polar=.true.)
  call add_card(header,'TTYPE5','TC      ')
  call add_card(header,'TTYPE6','GC      ')

  call add_card(header,'CODE','qml')
  call add_card(header,'NSIMS',numdata)
  call add_card(header,'LMAX',maxl)
  call add_card(header,'LSWITCH',switchl)

  call add_card(header,'DO_QML',do_qml)
  call add_card(header,'DECOUPLE',decouple_tp)

  
  fidfile = parse_string(handle,'clfiducial')
  call add_card(header,'FID_CLS',trim(fidfile))

  fidfile = parse_string(handle,'datafile')
  call add_card(header,'datafile',trim(fidfile))

  
  call parse_finish(handle)


  allocate(clsout(0:switchl,6,numdata))
  allocate(clserr(0:switchl,6))
  clsout = 0._dp
  clserr = 0._dp

  call cpu_time(t1)
  call get_qml_estimate(switchl,clsout(2:switchl,1:6,1:numdata),clserr(2:switchl,1:6))
  call cpu_time(t2)

  print*,'QML done in :',t2-t1

  do i =1,numdata
     write(istring,'(i5.5)') i
     call write_asctab(clsout(0:switchl,1:6,i),switchl,6,header,nlheader,trim(root)//istring//'_cls.fits')
     call write_asctab(clserr(0:switchl,1:6),switchl,6,header,nlheader,trim(root)//istring//'_err.fits')
  end do

end program qml
