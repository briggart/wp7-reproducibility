module debugging_mod

  implicit none
  logical ,parameter :: debugging = .false.

end module debugging_mod

module bflike_smw_mod

  use healpix_types
  use healpix_modules ,only : getsize_fits ,convert_ring2nest ,input_map  &
       ,fits2cl ,pix2vec_ring ,pix2vec_nest ,pixel_window ,read_dbintab   &
       ,printerror ,remove_dipole ,add_card ,write_bintabh ,read_bintab &
       ,del_card ,in_ring ,plm_gen ,pix2ang_ring
  use debugging_mod
  use bflike_types_mod
  use lpl_utils_mod 
  implicit none
  
  public init_pix_like ,clean_pix_like ,get_pix_loglike ,get_qml_estimate &
       ,get_qmlm_estimate ,get_template_coeff  &
       ,map2vec ,mask_matrix ,mask_vec &
       ,init_template_fit ,get_template_loglike_mcmc ,read_map &
       ,compute_plms

#ifdef GFORTRAN
 public update_ncvm ,precompute_rotation_angle ,compute_clnorm ,write_plms_to_fits ,get_pp_cov ,get_xx_cov ,get_tt_cov
#endif

  private

  interface

     module subroutine init_template_fit(parfile,T)

       implicit none
       character(len=*)      ,intent(in)  :: parfile
       class(tempfit_params) ,intent(out) :: T

     end subroutine

     module subroutine get_template_coeff(T,coeff,cov,chi2)

       implicit none
       class(tempfit_params) ,intent(in)  :: T
       real(dp) ,intent(out) ,allocatable :: coeff(:,:) ,cov(:,:) ,chi2(:,:)

     end subroutine

     module subroutine get_template_loglike_mcmc(P,D,coeff,lglike)

       implicit none
       type(fit_params_nl) ,intent(in)    :: P
       type(fit_data_nl)   ,intent(inout) :: D
       real(dp)            ,intent(in)    :: coeff(:)
       real(dp)            ,intent(out)   :: lglike

     end subroutine

     module subroutine compute_modes(vals,vecs,blocks,file,lstart)

       implicit none
       !compute legendre (associated) functions and mask them                  
       type(allocatable_matrix) ,intent(inout) :: vals(2:) ,vecs(2:) &
            ,blocks(2:,2:)
       character(len=*) ,intent(in) ,optional  :: file
       integer(i4b)     ,intent(in) ,optional  :: lstart

     end subroutine compute_modes
     
  end interface
   
  integer(i4b) ,parameter :: nlheader = 1024
  integer(i4b) ,save      :: ntemp ,ntot ,lmax ,lswitch ,ndata ,nq ,nqu ,nu &
       ,nsp = 0 ,off = 0

  real(dp) ,allocatable ,dimension(:,:) ,save :: clnorm ,dt ,auxdt ,cls ,S &
       ,N0 ,NCVM ,cos1 ,cos2 ,sin1 ,sin2 ,Tvec ,Qvec ,Uvec 
  real(dp) ,allocatable ,dimension(:)   ,save :: reflike ,pl,plm,f1,f2
  type(allocatable_matrix) ,allocatable ,save :: fevec(:) ,feval(:) ,cblocks(:,:)

  logical        ,save :: decouple_tp = .false. ,orthogonal_eb = .false. &
       ,has_t = .false. ,has_p = .false. ,has_x = .false.
  type(sky_mask) ,save :: mask
  
contains

  subroutine create_dataset(P)

    implicit none
    type(bf_params) ,intent(inout) :: P

    integer(i4b)           :: i ,unit ,il ,iu ,nwrite ,rcl
    real(dp) ,allocatable  :: dt(:,:) ,cov(:,:) ,map(:,:) ,v(:) ,bl(:,:) &
         ,hw(:,:)
    real(dp)               :: mondip(4)
    character(len=80)      :: header(nlheader) = '' ,bheader(nlheader) = ''
    character(len=8)       :: date
    character(len=10)      :: time
    character(filenamelen) :: filename ,lonstring

    !if no datafile is given, set a name based on date    
    if (trim(P%datafile) .eq. '') then
       call date_and_time(date=date,time=time)
       P%datafile = "datafile_bflike_"//date//"_"//time//".fits"
    end if
    
    ! read mask    
    call mask%set(P%maskfile)
    if(P%nside .ne. mask%s%nside)     stop "Mask has wrong nside"
    if(P%nstokes .ne. mask%s%nstokes) stop "Mask has wrong nstokes"

    P%lmax = min(P%lmax, 4*P%nside)
    
    ! add cards to header    
    call add_card(header,'NSIDE',P%nside)
    call add_card(header,'NSTOKES',P%nstokes)
    call add_card(header,'MASK',trim(P%maskfile))
    call add_card(header,'NTEMP',mask%ntemp)
    call add_card(header,'NQ',mask%nq)
    call add_card(header,'NU',mask%nu)
    call add_card(header,'NUMDATA',P%numdata)
    call add_card(header,'MAP',trim(P%mapfile))
    call add_card(header,'MAP_CF',P%map_cf)
    call add_card(header,'REMOVE_MD',P%remove_mondip)
    call add_card(header,'NCVM',trim(P%ncvmfile))
    call add_card(header,'NCVM_CF',P%ncvm_cf)
    call add_card(header,'BEAM',trim(P%beamfile))
    call add_card(header,'APPLY_PIXWIN',P%apply_pwf)

    !process data
    allocate(dt(mask%ntot,P%numdata))
    if(P%numdata .eq. 1) then
       open(newunit=unit,status='scratch')
       write(unit,'(a)') trim(lonstring)
       rewind(unit)
    else
       open(newunit=unit,file=trim(P%mapfile),status='old',action='read')
    end if

    do i = 1, P%numdata
       read(unit,'(a)') filename
       call read_map(filename,map,mask%s)

       if(P%remove_mondip) &
            call remove_dipole(P%nside,map(:,1),mask%s%ordering,2,mondip,[-1.d0,1.d0],mask=mask%m(:,1))

       v = map2vec(map)
       call mask_vec(mask%lmask,v)
       dt(:,i) = v

    end do
    close(unit) ;deallocate(map,v)
    dt = dt*P%map_cf

    !process the NCVM
    !assume ncvm is fortran direct for now
    allocate(cov(3*mask%s%npix,3*mask%s%npix))

    inquire(iolength=rcl) cov(:,1)
    open(newunit=unit,file=trim(P%ncvmfile),status='old',action='read',form='unformatted',access='direct',recl=rcl)
    do i = 1,3*mask%s%npix
       read(unit,rec=i) cov(:,i)
    end do
    close(unit)
    call mask_matrix(mask%lmask,cov)
    cov = cov*P%ncvm_cf
    
    !process the beam
    allocate(bl(0:4*P%nside,6),source=0._dp)
    call fits2cl(P%beamfile,bl,P%lmax,6,bheader)
    if(P%apply_pwf) then
       allocate(hw(0:4*P%nside,3))
       call pixel_window(hw,P%nside)
       bl(:,1) = bl(:,1)*hw(:,1)
       bl(:,2) = bl(:,2)*hw(:,2)
       bl(:,3) = bl(:,3)*hw(:,3)
       bl(:,4) = bl(:,4)*sqrt(hw(:,1)*hw(:,2))
       bl(:,5) = bl(:,5)*sqrt(hw(:,1)*hw(:,3))
       bl(:,6) = bl(:,6)*sqrt(hw(:,2)*hw(:,3))
       deallocate(hw)
    end if

    !find out how much stuff we need to write
    !pix vectors +symmetric NCVM +numdata maps +6 beam components (l>=2)
    nwrite = (3+P%numdata)*mask%ntot +(mask%ntot*(mask%ntot+1))/2 &
         +6*(4*P%nside-1)
    call add_card(header,'NWRITE',nwrite)

    !pack everything into single vector
    allocate(map(nwrite,1))

    iu = 0
    !coordinates
    do i = 1, mask%ntemp
       il = iu +1
       iu = il +2
       map(il:iu,1) = mask%Tvec(:,i)
    end do
    do i = 1, mask%nq
       il = iu +1
       iu = il +2
       map(il:iu,1) = mask%Qvec(:,i)
    end do
    do i = 1, mask%nu
       il = iu +1
       iu = il +2
       map(il:iu,1) = mask%Uvec(:,i)
    end do
       
    !beam
    do i = 1, 6
       il = iu +1
       iu = il +4*P%nside-2
       map(il:iu,1) = bl(2:4*P%nside,i)  
    end do

    !NCVM
    do i = 1, mask%ntot
       il = iu +1
       iu = il +(mask%ntot -i)
       map(il:iu,1) = cov(i:mask%ntot,i)
    end do
       
    !maps
    do i = 1, P%numdata
       il = iu +1
       iu = il +(mask%ntot -1)
       map(il:iu,1) = dt(:,i)
    end do

    !write stuff to file
    if (P%datafile(1:1) .eq. '!') then
       call write_bintabh(map, int(nwrite,i8b), 1, header, nlheader, &
          trim(P%datafile),repeat=1,firstpix=0_i8b)
    else
       call write_bintabh(map, int(nwrite,i8b), 1, header, nlheader, &
            '!'//trim(P%datafile),repeat=1,firstpix=0_i8b)
    end if

    write(*,*) "Dataset has been saved in: "//trim(P%datafile(2:))
    
  end subroutine create_dataset

! ---------------------------------------------------------------------------
  
  subroutine read_dataset(P)

    implicit none
    type(bf_params) ,intent(inout) :: P

    integer(i4b)                 :: myunit ,nwrite ,iu ,il ,i ,blocksize &
         ,readwrite ,hdutype ,err ,nside
    real(dp) ,allocatable        :: tmp(:,:) ,bl(:,:)
    real(dp)                     :: nullval
    logical                      :: skip ,anynull
    character(len=80)            :: comment

    do myunit = 100,1000
       inquire(unit=myunit, opened=skip)
       if(.not. skip) exit
    end do

    readwrite  = 0 ;err = 0
    call ftopen(myunit,P%datafile,readwrite,blocksize,err)
    call stop_on_fits_error(err,'opening dataset file')

    call ftmrhd(myunit, +1, hdutype, err)
    call stop_on_fits_error(err,'HDR')

    call ftgkyj(myunit,'NSIDE',nside ,comment ,err)
    call stop_on_fits_error(err,'NSIDE')

    call ftgkyj(myunit,'NTEMP',ntemp ,comment ,err)
    call stop_on_fits_error(err,'NTEMP')

    call ftgkyj(myunit,'NQ',nq ,comment ,err)
    call stop_on_fits_error(err,'NQ')

    call ftgkyj(myunit,'NU',nu ,comment ,err)
    call stop_on_fits_error(err,'NU')

    if(P%use_datafile) then
       !it's possible to reconstruct lmask from pixel coordinates,
       !but this is easier
       call ftgkys(myunit,'MASK',P%maskfile ,comment ,err)
       call stop_on_fits_error(err,'MASK')
       call mask%set(P%maskfile)
    end if

    nqu  = nq +nu
    ntot = ntemp +nqu

    has_t = ntemp .gt. 0
    has_p = nqu   .gt. 0
    has_x = has_t .and. has_p

    !nsp = Number of Stokes Params
    nsp = 0      ;off = 0
    if (has_t) then
       off = 1
       nsp = nsp +1
    end if
    if (has_p) nsp = nsp +2
    
    call ftgkyj(myunit,'NUMDATA',ndata ,comment ,err)
    call stop_on_fits_error(err,'NUMDATA')

    call ftgkyj(myunit,'NWRITE',nwrite ,comment ,err)
    call stop_on_fits_error(err,'NWRITE')

    allocate(tmp(0:nwrite-1,1))
    call read_dbintab(P%datafile, tmp, nwrite, 1, nullval ,anynull)
    if(anynull) then
       write(*,*) 'Missing pixels have been given the value ',nullval
    end if

    !this is redundant with sky_mask, but will do for now
    allocate(Tvec(3,ntemp) ,Qvec(3,nq) ,Uvec(3,nu))

    iu = -1 !tmp starts at 0

    !coordinates
    do i = 1 ,ntemp
       il = iu +1
       iu = il +2
       Tvec(:,i) = tmp(il:iu,1)
    end do
    do i = 1 ,nq
       il = iu +1
       iu = il +2
       Qvec(:,i) = tmp(il:iu,1)
    end do
    do i = 1 ,nu
       il = iu +1
       iu = il +2
       Uvec(:,i) = tmp(il:iu,1)
    end do
       
    !beam
    allocate(bl(0:4*nside,6))
    do i = 1,6
       il = iu +1
       iu = il +4*nside-2
       bl(2:4*nside,i) = tmp(il:iu,1)
    end do
    allocate(clnorm(2:lmax,6))
    call compute_clnorm(bl,clnorm)
    deallocate(bl)
    
    allocate(ncvm(ntot,ntot))
    do i = 1 ,ntot
       il = iu +1
       iu = il +(ntot -i)
       ncvm(i:ntot,i) = tmp(il:iu,1) 
    end do
       
    allocate(dt(ntot,ndata))
    do i = 1 ,ndata
       il = iu +1
       iu = il +(ntot -1)
       dt(:,i) = tmp(il:iu,1) 
    end do
    deallocate(tmp)

    call ftclos(myunit, err)
    if (err > 0) call printerror(err)

    P%nside   = nside
    P%numdata = ndata
    
  end subroutine read_dataset

! ---------------------------------------------------------------------------
  
  subroutine init_pix_like(parfile,passlmax,passlswitch,numdata)

    implicit none
    character(len=*) ,intent(in)  :: parfile
    integer(i4b)     ,intent(out) :: passlmax ,passlswitch ,numdata
 
    integer(i4b)          :: i ,j ,l ,iu ,il ,l1 ,l2 ,nside ,info ,neigen
    real(dp) ,allocatable :: evec(:,:) ,clstmp(:,:)
    logical               :: do_qml 
    type(bf_params)       :: BFP
    
    call BFP%set(parfile)
    lmax    = BFP%lmax
    lswitch = BFP%lswitch
    if (.not. BFP%use_datafile) then
       call create_dataset(BFP)
    end if
    call read_dataset(BFP)

    nside   = BFP%nside
    ndata   = BFP%numdata
    do_qml  = BFP%do_qml

    if(lswitch .gt. 2.5*nside) then
       lswitch = 2.5*nside
       write(*,*) 'lswitch must be <= 2.5nside'
       write(*,*) 'setting lswitch  =',lswitch
    end if
    passlmax    = lmax
    passlswitch = lswitch
    numdata     = ndata

    if(nsp*(lswitch+1)**2 .gt. ntot) &
         stop 'number of modes must be lower than number of pixels: try a lower lswitch'

!   global quantities, used throught the code
    allocate(reflike(ndata) ,cls(2:lmax,6)) ;cls = 0._dp

!   temp storage used in code initialization
    allocate(pl(1:lmax)) ;allocate(plm ,mold = pl)
    allocate(f1(2:lmax)) ;allocate(f2  ,mold = f1)
    allocate(sin1(ntot,ntot) ,source = 0._dp) ;allocate(sin2 ,cos1 ,cos2 ,source = sin1)

!   precompute the rotation angles
    call precompute_rotation_angle(sin1,cos1,sin2,cos2)

!   update the NCVM
    allocate(clstmp(2:lmax,6), source = 0._dp) 
    call read_cls_from_file(trim(BFP%clfiducial),clstmp) 

    
    if(do_qml) then
       allocate(N0,source=NCVM)
       l1 = 2            ;l2 = lmax   !need fiducial S+N for QML
    else if(lswitch .lt. lmax) then
       l1 = lswitch +1   ;l2 = lmax   !fix high-ell for likelihood
    else
       l1 = 3            ;l2 = 2      !only need to project mono/dipole
    end if
    call update_ncvm(clstmp,l1,l2,NCVM,project_mondip=BFP%project_mondip)
    deallocate(clstmp)
    
    allocate(fevec(2:lswitch),feval(2:lswitch),cblocks(2:lswitch,2:6))
    do l=2,lswitch
       call fevec(l)%alloc(nrow=ntot,ncol=nsp*(2*l+1))
       call feval(l)%alloc(nrow=nsp*(2*l+1),ncol=1)
       do j=2,6
          call cblocks(l,j)%alloc(nrow=nsp*(2*l+1),ncol=nsp*(2*l+1))
       end do
    end do

    !look for plms eigenmodes
    if(.not.read_plms_from_fits(BFP%basisfile,feval,fevec,cblocks,l1)) then
       write(*,*) 'basis file not found/wrong number of modes'
       write(*,*) 'computing plms from scratch'
       if(len_trim(BFP%basisfile) .gt. 0) then
          write(*,*) 'computed plms will be saved in: '//trim(BFP%basisfile)
          write(*,*)size(feval),size(fevec),size(cblocks)
          call compute_modes(feval,fevec,cblocks,trim(BFP%basisfile))
       else
          call compute_modes(feval,fevec,cblocks)
       end if
    elseif (l1 .lt. lswitch) then
       call compute_modes(feval,fevec,cblocks,trim(BFP%basisfile),l1+1)       
    end if

    !clean up stuff we don't need anymore
    deallocate(Tvec,Uvec,Qvec,sin1,cos1,sin2,cos2)
    deallocate(pl,plm,f1,f2,clnorm)

    if(do_qml) then
       call move_alloc(to=S,from=NCVM)
    else
       !contract NCVM and data on the plms 
       neigen = nsp*(lswitch+3)*(lswitch-1) !nsp*(2l+1) from 2 to lswitch 

       allocate(evec(ntot,neigen))
       iu = 0
       do l=2,lswitch
          il = iu +1
          iu = il +nsp*(2*l +1)-1
          evec(:,il:iu) = fevec(l)%m
       end do
       S     = evec
       auxdt = dt

       call dposv('L',ntot,ndata,ncvm,ntot,auxdt,ntot,info)
       if(info .ne. 0) then 
          write(*,*) '(updated) NCVM is not invertible'
          write(*,*) 'info = ',info
          stop
       end if
       
       do i = 1,ndata
          reflike(i) = sum(dt(:,i)*auxdt(:,i))
       end do
       if(debugging) write(*,*) 'reflike =',reflike(1)
       
       deallocate(dt)  ;allocate(dt(neigen,ndata))
       call dgemm('T','N', neigen, ndata, ntot, 1.d0, evec, &
            ntot, auxdt, ntot, 0.d0, dt, neigen)
       deallocate(auxdt) ;allocate(auxdt,source = dt)
       
       call dpotrs('L',ntot,neigen,NCVM,ntot,evec,ntot,info)
       write(*,*) 'info = ',info
       
       if(debugging) then
          write(*,*) '--------------------------'
          write(*,*) ' done V A^-1 d  '
          write(*,*) '--------------------------'
       end if
       
       deallocate(NCVM) ;allocate(NCVM(neigen,neigen))
       call dgemm('T','N', neigen, neigen, ntot, 1.d0, S, &
            ntot, evec, ntot, 0.d0, NCVM , neigen)
       
       if(debugging) then
          write(*,*) '--------------------------'
          write(*,*) ' done V A^-1 V^t  '
          write(*,*) '--------------------------'
       end if

       if(allocated(S)) deallocate(S) ;allocate(S, mold = NCVM)

       !eigenvectors are not needed for likelihood
       do l=2,lswitch
          call fevec(l)%clean()
       end do
       deallocate(fevec)
    end if


    if(debugging) then
       write(*,*) '--------------------------'
       write(*,*) ' done init_pix_like   '
       write(*,*) '--------------------------'
    end if
    
  end subroutine init_pix_like

! ---------------------------------------------------------------------------
  
  subroutine compute_clnorm(bl,clnorm)

    implicit none
    real(dp) ,intent(in)  :: bl(0:,:)
    real(dp) ,intent(out) :: clnorm(2:,:)

    integer(i4b)          :: l
    real(dp)              :: ell ,fct ,fct2 ,chngconv

    do l=2,lmax
       ell = real(l,kind=dp)

       !input cls are l(l+1)/2pi, we need (2l+1)/4pi
       chngconv = (2.d0*ell +1.d0)/2.d0/(ell*(ell+1.d0))

       !TP function is proportional to P_l2
       !F_X = sqrt((l-2)!/(l+2)!)*P_l2
!       fct2 = 1.d0/((ell+2.d0)*(ell+1.d0)*ell*(ell-1.d0))
       fct2 = 1.d0
       fct  = sqrt(fct2)

       clnorm(l,myTT) = chngconv*bl(l,myTT)**2
       clnorm(l,myEE) = chngconv*bl(l,myEE)**2*fct2
       clnorm(l,myBB) = chngconv*bl(l,myBB)**2*fct2
       clnorm(l,myTE) = chngconv*bl(l,myTE)**2*fct
       clnorm(l,myTB) = chngconv*bl(l,myTB)**2*fct
       clnorm(l,myEB) = chngconv*bl(l,myEB)**2*fct2

    end do

  end subroutine compute_clnorm

! ---------------------------------------------------------------------------

  subroutine compute_plms(vals,vecs,blocks,file,lstart)

    implicit none
    type(allocatable_matrix) ,intent(inout) :: vals(2:) ,vecs(2:) ,blocks(2:,2:)
    character(len=*) ,intent(in) ,optional :: file
    integer(i4b)     ,intent(in) ,optional :: lstart
    
    integer(i4b) ,allocatable :: isupp(:) ,iwork(:)
    integer(i4b)              :: il ,iu ,l ,lwork ,liwork ,info ,nevl ,i ,j &
         ,nl0 ,nl ,lqrwork ,maxev ,E_start ,E_stop ,B_start ,B_stop ,lstr
    real(dp) ,allocatable     :: work(:) ,evec(:,:) ,eval(:) ,tm1(:,:) &
         ,clstmp(:,:) ,tm2(:,:) ,qrwork(:) ,tau(:)
    double precision dlamch
    real(dp)                  :: sumS ,sumT ,sumD
    external dlamch
    
    if (present(lstart)) then
       lstr = lstart
    else
       lstr = 2
    end if
    
    if(.not. allocated(S)) allocate(S(ntot,ntot) ,source = 0._dp)
    allocate(clstmp(2:lmax,6))

    maxev = 2*lswitch+1
    if (has_t) then
    !TT plms and eigenvalues
       allocate(evec(ntemp,maxev),eval(ntemp),isupp(2*ntemp),work(1),iwork(1))
       call dsyevr('V', 'A', 'L', ntemp, S(1:ntemp,1:ntemp), ntemp, 0.d0, &
            0.d0, 0, 0, dlamch('S'), nevl, eval, evec, ntemp, isupp, &
            work, -1, iwork, -1, info)

       lwork = work(1)        ;liwork = iwork(1)

       deallocate(work,iwork) ;allocate(work(lwork),iwork(liwork))

       do l = lstr,lswitch
          if(debugging) print*,'computing TT basis l= ',l

          il = ntemp +1 -(2*l+1)    ;iu = ntemp
          clstmp = 0._dp            ;clstmp(l,myTT) = 1._dp

          call get_tt_cov(clstmp,l,l,S(1:ntemp,1:ntemp),project_mondip=.false.)

          call dsyevr('V', 'I', 'L', ntemp, S(1:ntemp,1:ntemp), ntemp, 0.d0, &
               0.d0, il, iu, 2*dlamch('S'), nevl, eval, evec, ntemp, &
               isupp, work, lwork, iwork, liwork, info)

          vecs(l)%m(1:ntemp,1:nevl) = evec(1:ntemp,1:nevl)
          vals(l)%m(1:nevl,1)       = eval(1:nevl)
          
       end do
       deallocate(evec,eval,isupp,work,iwork)
    end if

    !EE and BB plms and eigenvalues
    if (has_p) then    
       allocate(evec(nqu,maxev),eval(nqu),isupp(2*nqu),work(1),iwork(1))
       call dsyevr('V', 'A', 'L', nqu, S(ntemp+1:ntot,1+ntemp:ntot), nqu, &
            0.d0, 0.d0, 0, 0, dlamch('S'), nevl, eval, evec, nqu, isupp, &
            work, -1, iwork, -1, info)
       lwork = work(1) ;liwork = iwork(1)
       deallocate(work,iwork) ;allocate(work(lwork),iwork(liwork))

       S  = 0.d0
       do l = lstr,lswitch
          if(debugging) print*,'computing PP basis l= ',l
          nl0 = 2*l+1
          il = nqu +1 -nl0     ;iu = nqu
          
          !EE
          clstmp = 0._dp           ;clstmp(l,myEE) = 1._dp

          call get_pp_cov(clstmp, l, l, S(1+ntemp:ntot,1+ntemp:ntot))

          call dsyevr('V', 'I', 'L', nqu, S(1+ntemp:ntot,1+ntemp:ntot), nqu, &
               0.d0, 0.d0, il, iu, 2*dlamch('S'), nevl, eval, evec, nqu, &
               isupp, work, lwork, iwork, liwork, info)

          E_start = 1 +off*nl0
          E_stop  = E_start +(nevl-1)
          
          vecs(l)%m(ntemp+1:ntot,E_start:E_stop) = evec(1:nqu,1:nevl)
          vals(l)%m(E_start:E_stop,1)            = eval(1:nevl)
          blocks(l,myEE)%m = 0.d0

          do i    = E_start ,E_stop
             blocks(l,myEE)%m(i,i) = vals(l)%m(i,1)
          end do
          
          !BB
          clstmp = 0._dp           ;clstmp(l,myBB) = 1._dp
          call get_pp_cov(clstmp, l, l, S(1+ntemp:ntot,1+ntemp:ntot))

          call dsyevr('V', 'I', 'L', nqu, S(1+ntemp:ntot,1+ntemp:ntot), nqu, &
               0.d0, 0.d0, il, iu, 2*dlamch('S'), nevl, eval, evec, nqu, &
               isupp, work, lwork, iwork, liwork, info)

          B_start = 1 +(off+1)*nl0
          B_stop  = B_start +(nevl -1)

          vecs(l)%m(ntemp+1:ntot,B_start:B_stop) = evec(1:nqu,1:nevl)
          vals(l)%m(B_start:B_stop,1)            = eval(1:nevl)

          blocks(l,myBB)%m = 0._dp
          do i = B_start,B_stop
             blocks(l,myBB)%m(i,i) = vals(l)%m(i,1)
          end do

          if(orthogonal_EB) then
             allocate(tm1,source=vecs(l)%m(ntemp+1:ntot,off*nevl+1:(2+off)**nevl))
             allocate(tau(2*nevl) ,qrwork(1))
             call dgeqrf( nqu, 2*nevl, tm1, nqu, tau, qrwork, -1, INFO )
             lqrwork = qrwork(1)
             deallocate(qrwork); allocate(qrwork(lqrwork))
             call dgeqrf( nqu, 2*nevl, tm1, nqu, tau, qrwork, lqrwork, INFO)
             allocate(tm2(2*nevl,2*nevl))
             if(info .ne. 0) then
                write(*,*) 'QR factorization for l = ',l
                write(*,*) 'info =  ',info
                stop
             end if
             
             !copy R into tm2
             do i=1,2*nevl
                do j=1,i
                   tm2(j,i) = tm1(j,i) 
                end do
                tm2(i+1:2*nevl,i) = 0.d0
             end do
             
             !now create orthonormal base for EE-BB
             call dorgqr(nqu, 2*nevl, 2*nevl, tm1, nqu, tau, qrwork, lqrwork, info)
             do i=1,2*nevl
                vecs(l)%m(ntemp+1:ntot,i+off*nevl) = tm1(:,i)
             end do
             
             !rotate the EE block on the new basis: EE' = R EE R^t
             deallocate(tm1) ;allocate(tm1(2*nevl,2*nevl)) 
             tm1 = 0._dp
             !          blocks(l,myEE)%m = 0.d0
             !          do i = nevl+1,2*nevl
             !             blocks(l,myEE)%m(i,i) = eigenval(l)%m(i,1)
             !          end do
             
             call dgemm('N','N', 2*nevl, 2*nevl, 2*nevl,  1.d0, tm2, &
                  2*nevl, blocks(l,myEE)%m(off*nevl+1:(2+off)*nevl,off*nevl+1:(2+off)*nevl) ,2*nevl, 0.d0 ,tm1, 2*nevl)
             
             call dgemm('N','T', 2*nevl, 2*nevl, 2*nevl,  1.d0, tm1, &
                  2*nevl, tm2, 2*nevl, 0.d0, blocks(l,myEE)%m(off*nevl+1:(2+off)*nevl,off*nevl+1:(2+off)*nevl), 2*nevl)

             !rotate the BB block on the new basis
             tm1 = 0._dp
             !          blocks(l,myBB)%m = 0._dp
             !          do i = 2*nevl+1,3*nevl
             !             blocks(l,myBB)%m(i,i) = eigenval(l)%m(i,1)
             !          end do
             
             call dgemm('N','N', 2*nevl, 2*nevl, 2*nevl,  1.d0, tm2, &
                  2*nevl, blocks(l,myBB)%m( off*nevl+1:(2+off)*nevl,off*nevl+1:(2+off)*nevl) ,2*nevl, 0.d0 ,tm1, 2*nevl)
             
             call dgemm('N','T', 2*nevl, 2*nevl, 2*nevl,  1.d0, tm1, &
                  2*nevl, tm2, 2*nevl, 0.d0, blocks(l,myBB)%m(off*nevl+1:(2+off)*nevl,off*nevl+1:(2+off)*nevl), 2*nevl)
             
             deallocate(qrwork,tm1,tm2,tau)

          end if
       end do
       deallocate(evec,eval,isupp,work,iwork)

    end if

    !now find XY spectra coefficients
    if (has_x) then
              
       do l= lstr,lswitch
          S  = 0.d0
          nl0 = 2*l+1         ;nl  = 3*nl0 

          !TE
          clstmp = 0._dp     ;clstmp(l,myTE) = 1._dp

          call get_xx_cov(clstmp, l, l, S, symmetrize=.true.)

          allocate(evec,source=vecs(l)%m)
          blocks(l,myTE)%m = 0._dp

          call dgemm('N','N', ntot, 2*nl0, ntot, 1.d0, S, ntot,&
               fevec(l)%m(:,1:2*nl0), ntot, 0.d0, evec(:,1:2*nl0) ,ntot)
          call dgemm('T','N', 2*nl0, 2*nl0, ntot, 1.d0, &
               vecs(l)%m(:,1:2*nl0), ntot, evec(:,1:2*nl0), ntot, 0.d0, &
               blocks(l,myTE)%m(1:2*nl0,1:2*nl0), 2*nl0)

          if(debugging) then
             allocate(tm1,source = S)

             call dgemm('N','N', ntot, 2*nl0, 2*nl0, 1.d0, vecs(l)%m(:,1:2*nl0), &
                  ntot, blocks(l,myTE)%m(1:2*nl0,1:2*nl0) , 2*nl0, 0.d0 ,evec(:,1:2*nl0), ntot)

             call dgemm('N','T', ntot, ntot, 2*nl0, 1.d0, evec(:,1:2*nl0), &
                  ntot, vecs(l)%m(:,1:2*nl0), ntot, 0.d0 ,tm1, ntot)

             write(*,*) 'TE l =',l
             sumS = sum(S**2)
             sumT = sum(tm1**2)
             sumD = sum((S-tm1)**2)/sumS

             write(*,*) 'S^2  =',sumS,'T^2 =',sumT,'d =',sumD
             deallocate(tm1)

             do i = 1,2*nl0
                do j = 1,2*nl0
                   write(1000+l,*) j,i,blocks(l,myTE)%m(j,i)
                end do
             end do
             close(1000+l)

          end if

          !TB
          S = 0._dp   
          evec = 0._dp
          clstmp = 0._dp             ;clstmp(l,myTB) = 1._dp     
          blocks(l,myTB)%m(:,:) = 0._dp
          call get_xx_cov(clstmp,l,l,S,symmetrize=.true.)
          
          allocate(tm1(nl,nl))
          tm1 = 0._dp
          allocate(tm2,source=vecs(l)%m)
          if(.not. orthogonal_Eb) tm2(:,nl0+1:2*nl0) = 0._dp
          call dgemm('N','N', ntot, nl, ntot, 1.d0, S, ntot,&
               tm2, ntot, 0.d0, evec(:,1:nl) ,ntot)
          call dgemm('T','N', nl, nl, ntot, 1.d0, tm2, &
               ntot, evec(:,1:nl), ntot, 0.d0, tm1, nl)
          deallocate(tm2)
          
          blocks(l,myTB)%m(nl0+1:nl,1:nl0) = tm1(nl0+1:nl,1:nl0)
          blocks(l,myTB)%m(1:nl0,nl0+1:nl) = tm1(1:nl0,nl0+1:nl)

          if(debugging) then
             allocate(tm2,source = S)
             if (orthogonal_eb) then
                call dgemm('N','N', ntot, nl, nl, 1.d0, vecs(l)%m(:,1:nl), &
                     ntot, tm1(1:nl,1:nl) , nl, 0.d0 ,evec(:,1:nl), ntot)
                call dgemm('N','T', ntot, ntot, nl, 1.d0, evec(:,1:nl), &
                     ntot, vecs(l)%m(:,1:nl), ntot, 0.d0 ,tm2, ntot)
             else
                call dgemm('N','N', ntot, nl, nl, 1.d0, vecs(l)%m(:,1:nl), &
                     ntot, tm1(1:nl,1:nl) , nl, 0.d0 ,evec(:,1:nl), ntot)
                call dgemm('N','T', ntot, ntot, nl, 1.d0, evec(:,1:nl), &
                     ntot, vecs(l)%m(:,1:nl), ntot, 0.d0 ,tm2, ntot)
             end if
             write(*,*) 'TB l =',l
             sumS = sum(S**2)
             sumT = sum(tm2**2)
             sumD = sum((S-tm2)**2)/sumS
             write(*,*) 'S^2  =',sumS,'T^2 =',sumT,'d =',sumD
             deallocate(tm2)
             do i = 1,2*nl0
                do j = 1,2*nl0
                   write(2000+l,*) j,i,blocks(l,myTB)%m(j,i)
                end do
             end do
             close(2000+l)
          end if

          deallocate(tm1)
          deallocate(evec)
       end do
    end if
       
    if (has_p) then
       !EB
       do l = lstr,lswitch
          nl0 = 2*l+1
          allocate(evec,source=vecs(l)%m)
          S = 0._dp
          evec   = 0._dp
          clstmp = 0._dp
          clstmp(l,myEB) = 1._dp
          call get_pp_cov(clstmp, l, l, S(ntemp+1:ntot,ntemp+1:ntot), &
               symmetrize=.true.)

          call dgemm('N','N', nqu, 2*nl0, nqu, 1.d0, S(ntemp+1:ntot,ntemp+1:ntot),&
               nqu, vecs(l)%m(ntemp+1:ntot,off*nl0+1:(2+off)*nl0), nqu, 0.d0, &
               evec(ntemp+1:ntot,off*nl0+1:(2+off)*nl0) ,nqu)
          
          call dgemm('T','N', 2*nl0, 2*nl0, nqu, 1.d0, &
               vecs(l)%m(ntemp+1:ntot,off*nl0+1:(2+off)*nl0), nqu, &
               evec(ntemp+1:ntot,off*nl0+1:(2+off)*nl0), nqu, 0.d0, &
               blocks(l,myEB)%m(off*nl0+1:(2+off)*nl0,off*nl0+1:(2+off)*nl0), 2*nl0)
       

          if(debugging) then
             evec = 0._dp
             if(orthogonal_EB) then
                allocate(tm2,source = S)
                tm2  = 0._dp
                call dgemm('N','N', nqu, 2*nl0, 2*nl0, 1.d0, &
                     vecs(l)%m(ntemp+1:ntot,nl0+1:nl), nqu, &
                     blocks(l,myEB)%m(nl0+1:nl,nl0+1:nl), 2*nl0, &
                     0.d0 ,evec(ntemp+1:ntot,nl0+1:nl), nqu)
                
                call dgemm('N','T', nqu, nqu, 2*nl0, 1.d0,  &
                     evec(ntemp+1:ntot,nl0+1:nl), nqu, &
                     vecs(l)%m(ntemp+1:ntot,nl0+1:nl), nqu, &
                     0.d0 ,tm2(ntemp+1:ntot,ntemp+1:ntot), nqu)
                
                write(*,*) 'EB l =',l
                sumS = sum(S(ntemp+1:ntot,ntemp+1:ntot)**2)
                sumT = sum(tm2(ntemp+1:ntot,ntemp+1:ntot)**2)
                sumD = sum((S(ntemp+1:ntot,ntemp+1:ntot)-tm2(ntemp+1:ntot,ntemp+1:ntot))**2)/sumS
                write(*,*) 'S^2  =',sumS,'T^2 =',sumT,'d =',sumD
                deallocate(tm2)
             else
                write(*,*) 'for proper EB computation, set orthogonal_eb = T'
             end if
             do i = 1,2*nl0
                do j = 1,2*nl0
                   write(3000+l,*) j,i,blocks(l,myEB)%m(j,i)
                end do
             end do
             close(3000+l)
          end if

          deallocate(evec)
       end do
    end if

    deallocate(clstmp)
    if (present(file)) call write_plms_to_fits(file,clnorm,Tvec,Qvec,Uvec,vals,vecs,blocks)

  end subroutine compute_plms

! ---------------------------------------------------------------------------

  subroutine write_plms_to_fits(file,clnorm,Tvec,Qvec,Uvec,eval,evec&
       ,blocks)
    
    implicit none
    character(len=*)         ,intent(in) :: file
    real(dp)                 ,intent(in) :: clnorm(2:,:) ,Tvec(:,:) &
         ,Qvec(:,:) ,Uvec(:,:)
    type(allocatable_matrix) ,intent(in) :: eval(2:) ,evec(2:) ,blocks(2:,2:)

    integer(i4b)                         :: flsw ,l ,fntot ,nl ,j &
         ,extno ,lmax ,nfield ,nwrite
    character(len=80)                    :: header(nlheader) = ''
    character(filenamelen)               :: xfile
    character(:)            ,allocatable :: tag 
    character(len=6)                     :: dump 
    real(dp)                ,allocatable :: tmat(:,:)

    
    lmax = size(eval) +1
    
    call add_card(header,'NSIDE',mask%s%nside)
    call add_card(header,'NTEMP',ntemp)
    call add_card(header,'NQ'   ,nq)
    call add_card(header,'NU'   ,nu)
    call add_card(header,'LMAX' ,lmax)

    !always overwrite
    if (file(1:1) .eq. '!') then
       xfile = trim(file)
    else
       xfile = '!'//trim(file)
    end if

    !start writing

    !write normalization in first extension
    call add_card(header,'CONTAINS','clnorm')
    flsw  = lmax
    extno = 0
    nfield = 6
    do j = 1,nfield
       !need different names for each field to prevent healpy from getting all workedup
       write(dump,'(I0)') j
       tag = trim(adjustl(dump))
       call add_card(header,'TTYPE'//tag,'data_'//tag)
    end do
    call write_bintabh(clnorm(2:lmax,1:6), int(lmax-1,i8b), nfield, header, nlheader, xfile, &
         extno=extno)
    
    !write coordinates in second extension
    call add_card(header,'CONTAINS','coords',update=.true.)
    allocate(tmat(3,0:ntot-1))
    extno = 1
    tmat(:,0:ntemp-1)        = Tvec(:,1:ntemp)
    tmat(:,ntemp:ntemp+nq-1) = Qvec(:,1:nq)
    tmat(:,ntemp+nq:ntot-1)  = Uvec(:,1:nu)
    nfield = 3
    do j = 1,nfield
       write(dump,'(I0)') j
       tag = trim(adjustl(dump))
       call add_card(header,'TTYPE'//tag,'data_'//tag,update=.true.)
    end do
!    tmat = transpose(tmat)
    call write_bintabh(transpose(tmat), int(ntot,i8b), nfield, header, nlheader, &
         xfile, extno=extno ,repeat=ntot)
    deallocate(tmat)
    
    !now write each l in a different extenstion
    !blocks are mostly 0, so this is inefficient, but most space is taken
    !up by eigenvectors so this should do for now
    do l = 2,lmax
       extno = extno+1 
       call add_card(header,'ELL',l,update=.true.)
       nl = 2*l+1
       if(has_x) then
          flsw  = 3*nl
          fntot = ntot +6*flsw
          allocate(tmat(0:fntot-1,flsw),source = 0._dp)
          do j = 1,nl
             tmat(j-1,j) = eval(l)%m(j,1)
          end do
          do j = 2,6
             tmat((j-1)*flsw:j*flsw-1,1:flsw) = blocks(l,j)%m(1:flsw,1:flsw)
          end do
          tmat(6*flsw:fntot-1,:)   = evec(l)%m
          nwrite = int(fntot,i8b)*int(flsw,i8b)
          tmat = reshape(tmat(0:fntot-1,1:flsw),[nwrite,1])
 
          call add_card(header,'FNTOT',fntot,update=.true.)
          call add_card(header,'FLSW',flsw,update=.true.)
          call write_bintabh(tmat, int(nwrite,i8b), 1, header, &
               nlheader, xfile, extno=extno ,repeat=nwrite)
       elseif (has_p) then      
          flsw = 2*nl
          !we need only EE ,BB and EB blocks
          fntot = ntot +3*flsw
          allocate(tmat(0:fntot-1,flsw),source = 0._dp)

          tmat(0:flsw-1,:)        = blocks(l,myEE)%m
          tmat(flsw:2*flsw-1,:)   = blocks(l,myBB)%m
          tmat(2*flsw:3*flsw-1,:) = blocks(l,myEB)%m
          tmat(3*flsw:fntot-1,:)  = evec(l)%m
          nwrite = fntot*flsw
          tmat = reshape(tmat(0:fntot-1,1:flsw),[nwrite,1])
          call add_card(header,'FNTOT',fntot,update=.true.)
          call add_card(header,'FLSW',flsw,update=.true.)
          call write_bintabh(tmat, int(nwrite,i8b), 1, header, &
               nlheader, xfile, extno=extno ,repeat=nwrite)
       else
          flsw = nl
          !we need only TT
          fntot = ntot +flsw
          allocate(tmat(0:fntot-1,flsw),source = 0._dp)
          do j = 1,nl
             tmat(j-1,j) = eval(l)%m(j,1)
          end do
          tmat(flsw:fntot-1,:)   = evec(l)%m
          nwrite = fntot*flsw
          tmat = reshape(tmat(0:fntot-1,1:flsw),[nwrite,1])
          call add_card(header,'FNTOT',fntot,update=.true.)
          call add_card(header,'FLSW',flsw,update=.true.)
          call write_bintabh(tmat, int(nwrite,i8b), 1, header, &
               nlheader, xfile, extno=extno ,repeat=nwrite)
       end if
       deallocate(tmat)
    end do

  end subroutine write_plms_to_fits

! ---------------------------------------------------------------------------

  logical function read_plms_from_fits(file, eval, evec, blocks ,lfound)&
       result(OK)

    implicit none
    character(len=*)        ,intent(in)   :: file
    type(allocatable_matrix),intent(inout):: eval(2:) ,evec(2:) ,blocks(2:,2:)
    integer(i4b)            ,intent(out)  :: lfound

    real(dp)  ,allocatable :: tmat(:,:) ,scale(:,:) ,xtra(:,:) 
    real(dp)               :: nullval ,test
    integer(i4b)           :: flsw ,l ,fntot ,nl ,j ,extno ,lmax ,ltodo &
         ,myunit ,blocksize ,err ,readwrite ,hdutype ,ndum
    integer(i8b)           :: nread
    character(len=80)      :: comment
    logical                :: skip ,anynull
    
    ok = .false.
    
    do myunit = 100,1000
       inquire(unit=myunit, opened=skip)
       if(.not. skip) exit
    end do

    readwrite  = 0 ;err = 0
    call ftopen(myunit,file,readwrite,blocksize,err)
    if (err .ne. 0 ) return

    call ftmrhd(myunit, +1, hdutype, err)
    if (err .ne. 0 ) return

    call ftgkyj(myunit,'NTEMP',ndum ,comment ,err)
    if (err .ne. 0 .or. ndum .ne. ntemp) return

    call ftgkyj(myunit,'NQ',ndum ,comment ,err)
    if (err .ne. 0 .or. ndum .ne. nq) return

    call ftgkyj(myunit,'NU',ndum ,comment ,err)
    if (err .ne. 0 .or. ndum .ne. nu) return

    call ftgkyj(myunit,'LMAX',lfound ,comment ,err)
    call stop_on_fits_error(err,'LMAX')

    lmax  = size(eval) +1
    ltodo = min(lfound,lmax)

    allocate(scale(2:lfound,6))

    !read normalization
    extno = 0

    call read_bintab(file, scale(2:lfound,1:6), lfound-1, 6, nullval ,anynull ,extno=extno)
    scale(2:ltodo,1:6) = clnorm(2:ltodo,1:6)/scale(2:ltodo,1:6)
    where (.not.(abs(scale) .lt. huge(1.d0))) scale = 0._dp
    !read coordinates
    extno = 1
    allocate(tmat(ntot,3))
    call read_bintab(file, tmat, ntot, 3, nullval ,anynull ,extno=extno)
    if(has_t) then 
       test = sum(abs(transpose(tmat(1:ntemp,1:3)) -Tvec(1:3,1:ntemp)))
       if (test .gt. 1.d-12) then
          write(*,*) 'stored basis T mask does not match'
          return
       end if
    end if
    if(has_p) then
       test = sum(abs(transpose(tmat(ntemp+1:ntemp+nq,1:3)) -Qvec(1:3,1:nq)))
       if (test .gt. 1.d-12) then
          write(*,*) 'stored basis Q mask does not match'
          return
       end if
       test = sum(abs(transpose(tmat(ntemp+nq+1:ntot,1:3)) -Uvec(1:3,1:nu)))
       if (test .gt. 1.d-12) then
          write(*,*) 'stored basis U mask does not match'
          return
       end if
    end if
    deallocate(tmat)
 
    do l = 2,ltodo
       !now read each l from a different extension
       extno = extno+1 
       nl    = 2*l+1
 
       if(has_x) then
          flsw  = 3*nl
          fntot = ntot +6*flsw
          nread = int(flsw,i8b)*int(fntot,i8b)
          allocate(xtra(0:nread-1,1))
          call read_bintab(file, xtra, nread, 1, nullval ,anynull &
               ,extno=extno)
          allocate(tmat(0:fntot-1,flsw),source=reshape(xtra,[fntot,flsw]))
          do j = 1,nl
             eval(l)%m(j,1) = tmat(j-1,j)*scale(l,1) 
          end do
          do j = 2,6
             blocks(l,j)%m  = tmat((j-1)*flsw:j*flsw-1,:)*scale(l,j)
          end do
          evec(l)%m = tmat(6*flsw:fntot-1,:)  

          do j = 1,nl
             eval(l)%m(j+nl,1) = blocks(l,myEE)%m(j+nl,j+nl)*scale(l,myEE) 
          end do
          do j = 1,nl
             eval(l)%m(j+2*nl,1) = blocks(l,myBB)%m(j+2*nl,j+2*nl)* &
                  scale(l,myBB) 
          end do
       elseif (has_p) then      
          flsw = 2*nl
          !we need only EE ,BB and EB blocks
          fntot = ntot +3*flsw
          nread = int(flsw,i8b)*int(fntot,i8b)
          allocate(xtra(0:nread-1,1))
          call read_bintab(file, xtra, nread, 1, nullval ,anynull &
               ,extno=extno)
          allocate(tmat(0:fntot-1,flsw),source=reshape(xtra,[fntot,flsw]))
!          allocate(tmat(0:fntot-1,flsw))
!          call read_bintab(file, tmat, int(fntot,i8b), flsw, nullval ,anynull &
!               ,extno=extno)
          blocks(l,myEE)%m = tmat(0:flsw-1,:)*scale(l,myEE)        
          blocks(l,myBB)%m = tmat(flsw:2*flsw-1,:)*scale(l,myBB)         
          blocks(l,myEB)%m = tmat(2*flsw:3*flsw-1,:)*scale(l,myEB) 
          evec(l)%m        = tmat(3*flsw:fntot-1,:) 
          do j = 1,nl
             eval(l)%m(j,1) = blocks(l,myEE)%m(j,j)*scale(l,myEE) 
          end do
          do j = 1,nl
             eval(l)%m(j+nl,1) = blocks(l,myBB)%m(j+nl,j+nl)* &
                  scale(l,myBB) 
          end do
       else
          flsw = nl
          !we need only TT
          fntot = ntot +flsw
          nread = int(flsw,i8b)*int(fntot,i8b)
          allocate(xtra(0:nread-1,1))
          call read_bintab(file, xtra, nread, 1, nullval ,anynull &
               ,extno=extno)
          allocate(tmat(0:fntot-1,flsw),source=reshape(xtra,[fntot,flsw]))
          do j = 1,nl
             eval(l)%m(j,1) = tmat(j-1,j)*scale(l,myTT) 
          end do
          evec(l)%m = tmat(flsw:fntot-1,:)
       end if
       deallocate(xtra,tmat)
    end do

    call ftclos(myunit, err)
    call stop_on_fits_error(err,'Closing fileplm file')
    
    ok = .true.
    
  end function read_plms_from_fits

! ---------------------------------------------------------------------------

  subroutine clean_pix_like
    implicit none

    if(allocated(S))        deallocate(S)
    if(allocated(clnorm))   deallocate(clnorm)
    if(allocated(cls))      deallocate(cls)
    if(allocated(pl))       deallocate(pl)
    if(allocated(plm))      deallocate(plm)
    if(allocated(f1))       deallocate(f1)
    if(allocated(f2))       deallocate(f2)
    if(allocated(dt))       deallocate(dt)
    if(allocated(auxdt))    deallocate(auxdt)

  end subroutine clean_pix_like

! ---------------------------------------------------------------------------

  subroutine get_tt_cov(clsin,linf,lsup,cov,project_mondip,symmetrize,update)

    implicit none
    real(dp) ,intent(in)          :: clsin(2:,:)
    integer ,intent(in)           :: linf ,lsup
    real(dp) ,intent(inout)       :: cov(:,:)
    logical ,optional ,intent(in) :: project_mondip ,symmetrize ,update

    logical  :: up
    integer  :: i ,j ,l
    real(dp) :: tt ,cz ,ct0 ,ct1

    ct0 = 0._dp
    ct1 = 0._dp

    if(present(project_mondip)) then
       if(project_mondip) then
          ct0 = 1.e6_dp
          ct1 = 1.e6_dp
       end if
    end if

    !default is overwrite 
    if(present(update)) then 
       up = update
    else
       up = .false.
    end if
    if(.not. up) cov = 0.d0
    
    cls(2:lsup,myTT) = clsin(2:lsup,myTT)*clnorm(2:lsup,myTT)
!$OMP PARALLELDO DEFAULT (PRIVATE) SHARED(cov,Tvec,cls,ntemp,lsup,linf,ct1,ct0)
    do i=1,ntemp
! TT 
       cov(i,i) = cov(i,i) +sum(cls(linf:lsup,myTT)) +ct1 +ct0        
       do j=i+1,ntemp
          cz = sum(Tvec(:,j)*Tvec(:,i))
          pl(1) = cz
          pl(2) = 1.5d0*cz*cz -.5d0
          do l = 3,lsup
             pl(l) =(cz*(2*l -1)*pl(l-1) -(l-1)*pl(l-2))/l
          enddo
          tt = sum(cls(linf:lsup,myTT)*pl(linf:lsup)) &
               +Pl(1)*ct1 +ct0 !project out monopole and dipole
          cov(j,i) = cov(j,i) +tt
       enddo
    end do

    if (present(symmetrize)) then
       if(symmetrize) call symmetrize_matrix(cov)
    end if

  end subroutine get_tt_cov

! ---------------------------------------------------------------------------

  subroutine get_pp_cov(clsin,linf,lsup,cov,symmetrize,update)

    implicit none
    real(dp)          ,intent(in)    :: clsin(2:,:)
    integer           ,intent(in)    :: linf ,lsup
    real(dp)          ,intent(inout) :: cov(:,:)
    logical ,optional ,intent(in)    :: symmetrize ,update

    real(dp) ,allocatable :: fl(:) 
    real(dp)              :: qq ,uu ,qu ,cz ,c1c2 ,s1s2 ,c1s2 ,s1c2
    integer               :: i ,j ,iu ,iq ,ju ,jq
    logical               :: up 

    allocate(fl(2:lsup))
    do i = 2,lsup
       fl(i) = (i+2)*(i+1)*i*(i-1)
    end do
    cls(2:lsup,myEE) = clsin(2:lsup,myEE)*clnorm(2:lsup,myEE)/fl(2:lsup)
    cls(2:lsup,myBB) = clsin(2:lsup,myBB)*clnorm(2:lsup,myBB)/fl(2:lsup)
    cls(2:lsup,myEB) = clsin(2:lsup,myEB)*clnorm(2:lsup,myEB)/fl(2:lsup)
    deallocate(fl)
    
    !default is overwrite 
    if(present(update)) then 
       up = update
    else
       up = .false.
    end if
    if(.not. up) cov = 0.d0

!$OMP PARALLEL DEFAULT (PRIVATE) SHARED(cov,Qvec,Uvec,cls,nu,nq,ntemp,lsup,linf,cos1,cos2,sin1,sin2)
!$OMP DO
    do i = 1,nq
!QQ 
       iq = i +ntemp
       do j = i,nq
          cz = sum(Qvec(:,j)*Qvec(:,i))
          call compute_polfunc(cz,plm,f1,f2,qq,uu,qu)

          jq = j +ntemp

          c1c2 = cos1(jq,iq)*cos2(jq,iq)
          s1s2 = sin1(jq,iq)*sin2(jq,iq)
          c1s2 = cos1(jq,iq)*sin2(jq,iq)
          s1c2 = sin1(jq,iq)*cos2(jq,iq)

          cov(j,i) = cov(j,i) +qq*c1c2 +uu*s1s2 +qu*(c1s2+s1c2)

       end do

 !UQ 
       do j=1,nu
          cz = sum(Uvec(:,j)*Qvec(:,i))
          call compute_polfunc(cz,plm,f1,f2,qq,uu,qu)

          ju = j+nq+ntemp

          c1c2 = cos1(ju,iq)*cos2(ju,iq)
          s1s2 = sin1(ju,iq)*sin2(ju,iq)
          c1s2 = cos1(ju,iq)*sin2(ju,iq)
          s1c2 = sin1(ju,iq)*cos2(ju,iq)

          cov(j+nq,i) = cov(j+nq,i) -qq*s1c2 +uu*c1s2 +qu*(c1c2 -s1s2)
       end do
    end do

!$OMP DO
    do i =1,nu
!UU 
       iu = i+nq+ntemp
       do j=i,nu
          cz = sum(Uvec(:,j)*Uvec(:,i))
          call compute_polfunc(cz,plm,f1,f2,qq,uu,qu)

          ju = j+nq+ntemp

          c1c2 = cos1(ju,iu)*cos2(ju,iu)
          s1s2 = sin1(ju,iu)*sin2(ju,iu)
          c1s2 = cos1(ju,iu)*sin2(ju,iu)
          s1c2 = sin1(ju,iu)*cos2(ju,iu)

          cov(j+nq,i+nq) = cov(j+nq,i+nq) +qq*s1s2 +uu*c1c2 -qu*(c1s2 +s1c2)

       end do
    end do
!$OMP END PARALLEL

    if (present(symmetrize)) then
       if(symmetrize) call symmetrize_matrix(cov)
    end if

    if(debugging  .and. .not.debugging)  then

!$OMP PARALLEL DEFAULT (PRIVATE) SHARED(cov,Qvec,Uvec,cls,nu,nq,ntemp,lsup,linf,cos1,cos2,sin1,sin2)
!$OMP DO
       do i = 1,nq
!QQ 
          iq = i +ntemp
          do j = 1,i-1
             cz = sum(Qvec(:,j)*Qvec(:,i))
             call compute_polfunc(cz,plm,f1,f2,qq,uu,qu)
             
             jq = j +ntemp
             
             c1c2 = cos1(jq,iq)*cos2(jq,iq)
             s1s2 = sin1(jq,iq)*sin2(jq,iq)
             c1s2 = cos1(jq,iq)*sin2(jq,iq)
             s1c2 = sin1(jq,iq)*cos2(jq,iq)
             
             cov(j,i) = cov(j,i) +qq*c1c2 +uu*s1s2 +qu*(c1s2+s1c2)
             
          end do
       end do
       
!$OMP DO      
       do i =1,nu
 !UQ 
          iu = i+nq+ntemp
          
          do j=1,nq
             cz = sum(Qvec(:,j)*Uvec(:,i))
             call compute_polfunc(cz,plm,f1,f2,qq,uu,qu)
             
             jq = i +ntemp
             
             c1c2 = cos1(jq,iu)*cos2(jq,iu)
             s1s2 = sin1(jq,iu)*sin2(jq,iu)
             c1s2 = cos1(jq,iu)*sin2(jq,iu)
             s1c2 = sin1(jq,iu)*cos2(jq,iu)
             
             cov(j,i+nq) = cov(j,i+nq) -qq*c1s2 +uu*s1c2 +qu*(c1c2 -s1s2)
          end do
       
!UU 
          do j=1,i-i
             cz = sum(Uvec(:,j)*Uvec(:,i))
             call compute_polfunc(cz,plm,f1,f2,qq,uu,qu)
          
             ju = j+nq+ntemp
             c1c2 = cos1(ju,iu)*cos2(ju,iu)
             s1s2 = sin1(ju,iu)*sin2(ju,iu)
             c1s2 = cos1(ju,iu)*sin2(ju,iu)
             s1c2 = sin1(ju,iu)*cos2(ju,iu)
             
             cov(j+nq,i+nq) = cov(j+nq,i+nq) +qq*s1s2 +uu*c1c2 -qu*(c1s2 +s1c2)
             
          end do
       end do
!$OMP END PARALLEL
    end if

  contains

    subroutine compute_polfunc(cz,plm,f1,f2,qq,uu,qu)

      implicit none
      real(dp) ,intent(in)  :: cz
      real(dp) ,intent(out) :: plm(:),f1(2:),f2(2:),qq,uu,qu

      integer               :: l

      !do recursion on plm/(1-z^2) to avoid division by 0
      plm(1) = 0.d0
      plm(2) = 3.d0
      f1(2)  = 6.d0*(1d0+cz*cz)
      f2(2)  = -12.d0*cz
      do l = 3,lsup
         plm(l) =(cz*(2*l -1)*plm(l-1) -(l+1)*plm(l-2))/(l -2)
         f1(l) =-(2*l-8 +l*(l-1)*(1.d0 -cz*cz))*plm(l)+ &
              (2*l+4)*cz*plm(l-1)
         f2(l) = 4.d0*(-(l-1)*cz*plm(l) +(l+2)*plm(l-1))
      enddo

      qq = sum(cls(linf:lsup,myEE)*f1(linf:lsup) &
           -cls(linf:lsup,myBB)*f2(linf:lsup))
      uu = sum(cls(linf:lsup,myBB)*f1(linf:lsup) &
           -cls(linf:lsup,myEE)*f2(linf:lsup))
      qu = sum((f1(linf:lsup) +f2(linf:lsup))*cls(linf:lsup,myEB))
      
    end subroutine compute_polfunc
    
  end subroutine get_pp_cov

! ---------------------------------------------------------------------------

  subroutine get_xx_cov(clsin,linf,lsup,cov,symmetrize,update)

    implicit none
    real(dp)          ,intent(in)    :: clsin(2:,:)
    integer           ,intent(in)    :: linf ,lsup
    real(dp)          ,intent(inout) :: cov(:,:)
    logical ,optional ,intent(in)    :: symmetrize ,update

    real(dp) ,allocatable :: fl(:)
    real(dp)              :: tq ,tu ,cz
    logical               :: up
    integer               :: i ,j ,ju ,jq ,iq ,iu

    allocate(fl(2:lsup))
    do i =2,lsup
       fl(i) = sqrt(real((i+2)*(i+1)*i*(i-1),dp))
    end do
    cls(2:lsup,myTE) = clsin(2:lsup,myTE)*clnorm(2:lsup,myTE)/fl(2:lsup)
    cls(2:lsup,myTB) = clsin(2:lsup,myTB)*clnorm(2:lsup,myTB)/fl(2:lsup)
    deallocate(fl)
    
    !default is overwrite 
    if(present(update)) then 
       up = update
    else
       up = .false.
    end if
    if(.not. up) then
       cov(1:ntemp,ntemp+1:ntot) = 0.d0
       cov(ntemp+1:ntot,1:ntot)  = 0.d0
    end if

!$OMP PARALLELDO DEFAULT (PRIVATE) SHARED(cov,Tvec,Qvec,Uvec,cls,nu,nq,ntemp,lsup,linf,cos1,cos2,sin1,sin2)
    do i=1,ntemp
! QT
       do j=1,nq
          cz = sum(Qvec(:,j)*Tvec(:,i))
          call compute_tpfunc(cz,cls,plm,tq,tu)
          jq = j +ntemp
          cov(jq,i) = cov(jq,i) +tq*cos1(jq,i) +tu*sin1(jq,i) 

       enddo

!UT
       do j=1,nu
          cz = sum(Uvec(:,j)*Tvec(:,i))
          call compute_tpfunc(cz,cls,plm,tq,tu)
          ju = j +ntemp +nq
          cov(ju,i) = cov(ju,i) -tq*sin1(ju,i) +tu*cos1(ju,i) 

       enddo

    end do

    if (present(symmetrize)) then
       if (symmetrize) call symmetrize_matrix(cov)
    end if

    if(debugging .and.  .not.debugging) then
!$OMP PARALLEL DEFAULT (PRIVATE) SHARED(cov,Tvec,Qvec,Uvec,cls,nu,nq,ntemp,lsup,linf,cos1,cos2,sin1,sin2)
!$OMP DO
       do i=1,nq
!TQ
          iq = i +ntemp
          do j=1,ntemp
             cz = sum(Tvec(:,j)*Qvec(:,i))
             call compute_tpfunc(cz,cls,plm,tq,tu)
             cov(j,iq) = cov(j,iq) +tq*cos2(j,iq)  +tu*sin2(j,iq)
             
          enddo
       end do
!TU
!$OMP DO
       do i=1,nu
          iu = i+ntemp+nq
          do j=1,ntemp
             cz = sum(Tvec(:,j)*Uvec(:,i))
             call compute_tpfunc(cz,cls,plm,tq,tu)
             cov(j,iu) = cov(j,iu) -tq*sin2(j,iu) +tu*cos2(j,iu)
          enddo
       end do
!$OMP END PARALLEL
    end if

  contains

    subroutine compute_tpfunc(cz,cls,plm,tq,tu)

      implicit none
      real(dp) ,intent(in)  :: cz ,cls(2:,:)
      real(dp) ,intent(out) :: plm(:) ,tq ,tu

      integer               :: l

      plm(1) = 0.d0
      plm(2) = 3.d0*(1.d0 -cz*cz)
      do l = 3,lsup
         plm(l) =(cz*(2*l -1)*plm(l-1) -(l+1)*plm(l-2))/(l-2)
      enddo
      tq = -sum(cls(linf:lsup,myTE)*plm(linf:lsup))
      tu = -sum(cls(linf:lsup,myTB)*plm(linf:lsup))

    end subroutine compute_tpfunc
      
  end subroutine get_xx_cov

! ---------------------------------------------------------------------------

  subroutine get_pix_loglike_tt(clsin,alike,argexp,logdet)
!input clsin are l(l+1)C_l/2pi

    implicit none    
    real(dp),intent(in)  :: clsin(2:)
    real(dp),intent(out) :: alike(:),argexp(:),logdet(:)

    integer              :: i ,j ,l ,info ,neigen ,nl ,id
    real(dp)             :: tmp ,tmplog

    logdet = 0._dp
    neigen = size(dt(:,1))

    do concurrent (i = 1:neigen)
       S(i:neigen,i) = ncvm(i:neigen,i)
    end do

    id     = 0
    tmplog = 0._dp
    
    !TT is diagonal
    do l = 2,lswitch
       nl  = 2*l+1
       do i=1,nl
          id  = id +1
          tmp = clsin(l)*feval(l)%m(i,1)
          tmplog   = tmplog +log(abs(tmp))
          S(id,id) = S(id,id) +1._dp/tmp
       end do
    end do

    if(debugging) then
       write(*,*) '--------------------------'
       write(*,*) 'S +NCVM ,dt'
       do i = 1,15
          write(*,*) i,i,S(i,i),dt(i,1)
       end do
       write(*,*) '--------------------------'
    end if

    if(debugging) then
       write(*,*) '--------------------------'
       write(*,*) 'starting dposv '
       write(*,*) '--------------------------'
    end if

    auxdt = dt
    
    call dposv('L',neigen,ndata,S,neigen,auxdt,neigen,info)

    if(debugging) then
       write(*,*) '--------------------------'
       write(*,*) 'finished dposv '
       write(*,*) 'info = ',info
       write(*,*) '--------------------------'
    end if

!    cholesky: X*(C^-1*X)
    logdet(1) = tmplog/2
    if (info.eq.0) then

       do j=1,neigen
          logdet(1) = logdet(1) +log(S(j,j))
       enddo
       logdet(1) = 2.d0*logdet(1)

       logdet(2:ndata) = logdet(1)

!$OMP PARALLELDO DEFAULT(PRIVATE) SHARED(argexp,dt,auxdt,alike,logdet,reflike,ndata)
       do i=1,ndata
          argexp(i) = reflike(i) -sum(dt(:,i)*auxdt(:,i))
          alike(i)  = -.5d0*(argexp(i)+logdet(i))
       end do

    else

       argexp = 1.d30
       logdet = 1.d30

       alike = -.5d0*(argexp+logdet)
    endif

    if(debugging) then
       write(*,*) '--------------------------'
       do j=1,1
          write(*,*) 'argexp =',argexp(j)
          write(*,*) 'logdet =',logdet(j)
       end do
       write(*,*) '--------------------------'
    end if

  end subroutine get_pix_loglike_tt

! ---------------------------------------------------------------------------

  subroutine get_pix_loglike(clsin,alike,argexp,logdet)
    !input clsin are l(l+1)C_l/2pi ,conversions are handled internally
    implicit none

    real(dp),intent(in)  :: clsin(2:,:)
    real(dp),intent(out) :: alike(:),argexp(:),logdet(:)

    if (debugging) then
       write(*,*) '--------------------------'
       write(*,*) 'size = ' ,size(clsin(:,1)) ,size(clsin(2,:))
       write(*,*) 'ndata = ',ndata
       write(*,*) 'size = ',size(alike(:)),size(argexp(:)),size(logdet(:))
       write(*,*) '--------------------------'
    end if

    if (debugging) then
       write(*,*) '--------------------------'
       write(*,*) 'in get_pix_loglike'
       write(*,'(6e13.6)') clsin(2,:)
       write(*,'(6e13.6)') clsin(lmax-1,:)
       write(*,*) '--------------------------'
    end if

    if (has_x) then
       call  get_pix_loglike_tp(clsin,alike,argexp,logdet)
    elseif (has_t) then
       call  get_pix_loglike_tt(clsin(:,myTT),alike,argexp,logdet)
    else
       call  get_pix_loglike_pp(clsin(:,:),alike,argexp,logdet)
    end if
       
  end subroutine get_pix_loglike
  
! ---------------------------------------------------------------------------
  
  subroutine get_pix_loglike_tp(clsin,alike,argexp,logdet)

    implicit none
    
    real(dp),intent(in)  :: clsin(2:,:)
    real(dp),intent(out) :: alike(:),argexp(:),logdet(:)

    integer              :: i ,j ,l ,info ,neigen ,nl ,nl2 ,nl3 ,iu
    integer  ,allocatable:: pinfo(:) ,il(:)
    real(dp) ,allocatable:: tm1(:,:) ,tmplog(:)
    real(dp)             :: tmp

    logdet = 0._dp
    neigen = size(dt(:,1))

    nl2 = 2*(2*lswitch+1)
    allocate(tm1(nl2,nl2),tmplog(2:lswitch),pinfo(2:lswitch),il(2:lswitch))
    iu = 0
    do l=2,lswitch
       il(l) = iu +1
       iu = il(l) +6*l+2 !3*(2l+1) for each l
    end do

!$OMP PARALLELDO DEFAULT (PRIVATE) SHARED(S,ncvm,neigen)
    do i=1,neigen
       S(i:neigen,i) = ncvm(i:neigen,i)
    end do

!!!$OMP PARALLELDO DEFAULT(PRIVATE) SHARED(S,neigen,lswitch,clsin,tmplog,cblocks,feval,pinfo,il)
    do l=2,lswitch

       tmplog(l) = 0._dp
    
       nl  = 2*l+1
       nl2 = 2*nl
       nl3 = 3*nl
 
       ! TT,TE,EE 
       do i=1,nl
          tm1(i,i)        = clsin(l,myTT)*feval(l)%m(i,1)
          tm1(i+1:nl,i)   = 0.d0
          tm1(nl+1:nl2,i) = clsin(l,myTE)*cblocks(l,myTE)%m(nl+1:nl2,i)
       end do
       do i = nl+1,nl2
          tm1(i,i)        = clsin(l,myEE)*feval(l)%m(i,1)
          tm1(i+1:nl2,i)  = 0.d0
       end do

       call dpotrf('L',nl2,tm1(1:nl2,1:nl2),nl2,pinfo(l))

       do j=1,nl2
          tmplog(l) = tmplog(l) +log(tm1(j,j))
       end do
       call dpotri('L',nl2,tm1(1:nl2,1:nl2),nl2,info)

       do i=1,nl2
          do j=i,nl2
             S(il(l)-1+j,il(l)-1+i) = S(il(l)-1+j,il(l)-1+i) +tm1(j,i)
          end do
       end do

       !BB is diagonal 
       do i = nl2+1,nl3
          tmp = 1._dp/(max(clsin(l,myBB),1.d-30)*feval(l)%m(i,1))
          S(il(l)+i-1,il(l)+i-1) = S(il(l)+i-1,il(l)+i-1) +tmp
          tmplog(l) = tmplog(l) -.5_dp*log(tmp) 
       end do
 
    end do

    logdet(1) = sum(tmplog(2:lswitch))
    
    do l=2,lswitch
       if(pinfo(l).ne.0) then
          write(*,*) 'block inversion failed for l =',l,' info =',pinfo(l)
          argexp = -1.d30
          logdet = -1.d30
          alike  = -1.d30
          deallocate(tmplog,il,tm1,pinfo)
          if(debugging) stop
          return
       end if
    end do

    if(debugging) then
       write(*,*) '--------------------------'
       write(*,*) 'S +NCVM ,dt'
       do i = 1,15
          write(*,*) i,i,S(i,i),dt(i,1)
       end do
       write(*,*) '--------------------------'
    end if

    if(debugging) then
       write(*,*) '--------------------------'
       write(*,*) 'starting dposv '
       write(*,*) '--------------------------'
    end if

    auxdt = dt
    
    call dposv('L',neigen,ndata,S,neigen,auxdt,neigen,info)

    if(debugging) then
       write(*,*) '--------------------------'
       write(*,*) 'finished dposv '
       write(*,*) 'info = ',info
       write(*,*) '--------------------------'
    end if

!    cholesky: X*(C^-1*X)
    if (info.eq.0) then

       do j=1,neigen
          logdet(1) = logdet(1) +log(S(j,j))
       enddo
       logdet(1) = 2.d0*logdet(1)

       logdet(2:ndata) = logdet(1)

!$OMP PARALLELDO DEFAULT(PRIVATE) SHARED(argexp,dt,auxdt,alike,logdet,reflike,ndata)
       do i=1,ndata
          argexp(i) = reflike(i) -sum(dt(:,i)*auxdt(:,i))
          alike(i)  = -.5d0*(argexp(i)+logdet(i))
       end do

    else

       argexp = 1.d30
       logdet = 1.d30

       alike = -.5d0*(argexp+logdet)
    endif

    if(debugging) then
       write(*,*) '--------------------------'
       do j=1,1
          write(*,*) 'argexp =',argexp(j)
          write(*,*) 'logdet =',logdet(j)
       end do
       write(*,*) '--------------------------'
    end if

    write(*,*) argexp(1),logdet(1)

    deallocate(tmplog,il,tm1,pinfo)
  end subroutine get_pix_loglike_tp

! ---------------------------------------------------------------------------
  
  subroutine get_pix_loglike_pp(clsin,alike,argexp,logdet)

    implicit none
    
    real(dp),intent(in)  :: clsin(2:,:)
    real(dp),intent(out) :: alike(:) ,argexp(:) ,logdet(:)

    integer              :: i ,j ,l ,info ,neigen ,nl ,nl2 ,iu
    integer  ,allocatable:: pinfo(:) ,il(:)
    real(dp) ,allocatable:: tm1(:,:) ,tmplog(:)
!
    logdet = 0._dp
    neigen = size(dt(:,1))

    nl2 = 2*(2*lswitch+1)
    allocate(tm1(nl2,nl2) ,tmplog(2:lswitch) ,source = 0._dp)
    allocate(pinfo(2:lswitch) ,il(2:lswitch) ,source = 0)
    iu = 0
    do l=2,lswitch
       il(l) = iu +1
       iu = il(l) +4*l+1 !2*(2l+1) for each l
    end do

!$OMP PARALLELDO DEFAULT (PRIVATE) SHARED(S,ncvm,neigen)
    do i=1,neigen
       S(i:neigen,i) = ncvm(i:neigen,i)
    end do

!!!$OMP PARALLELDO DEFAULT(PRIVATE) SHARED(S,neigen,lswitch,clsin,tmplog,cblocks,feval,pinfo,il)
    do l=2,lswitch

       tmplog(l) = 0._dp
    
       nl  = 2*l+1
       nl2 = 2*nl

       ! EE, BB
       ! This is diagonal, but keep matrix structure to include EB later
       do i = 1,nl
          tm1(i,i)        = clsin(l,myEE)*feval(l)%m(i,1)
          tm1(i+1:nl2,i)  = 0.d0
       end do
       do i = nl+1,nl2
          tm1(i,i)        = clsin(l,myBB)*feval(l)%m(i,1)
          tm1(i+1:nl2,i)  = 0.d0
       end do

       call dpotrf('L',nl2,tm1(1:nl2,1:nl2),nl2,pinfo(l))

       do j=1,nl2
          tmplog(l) = tmplog(l) +log(tm1(j,j))
       end do
       call dpotri('L',nl2,tm1(1:nl2,1:nl2),nl2,info)

       do i=1,nl2
          do j=i,nl2
             S(il(l)-1+j,il(l)-1+i) = S(il(l)-1+j,il(l)-1+i) +tm1(j,i)
          end do
       end do

    end do

    logdet(1) = sum(tmplog(2:lswitch))
    
    do l=2,lswitch
       if(pinfo(l).ne.0) then
          write(*,*) 'block inversion failed for l =',l,' info =',pinfo(l)
          argexp = -1.d30
          logdet = -1.d30
          alike  = -1.d30
          deallocate(tmplog,il,tm1,pinfo)
          if(debugging) stop
          return
       end if
    end do

    if(debugging) then
       write(*,*) '--------------------------'
       write(*,*) 'S +NCVM ,dt'
       do i = 1,min(15,size(S(:,1)))
          write(*,*) i,i,S(i,i),dt(i,1)
       end do
       write(*,*) '--------------------------'
    end if

    if(debugging) then
       write(*,*) '--------------------------'
       write(*,*) 'starting dposv '
       write(*,*) '--------------------------'
    end if

    auxdt = dt
    
    call dposv('L',neigen,ndata,S,neigen,auxdt,neigen,info)

    if(debugging) then
       write(*,*) '--------------------------'
       write(*,*) 'finished dposv '
       write(*,*) 'info = ',info
       write(*,*) '--------------------------'
    end if

!    cholesky: X*(C^-1*X)
    if (info.eq.0) then

       do j=1,neigen
          logdet(1) = logdet(1) +log(S(j,j))
       enddo
       logdet(1) = 2.d0*logdet(1)

       logdet(2:ndata) = logdet(1)

!$OMP PARALLELDO DEFAULT(PRIVATE) SHARED(argexp,dt,auxdt,alike,logdet,reflike,ndata)
       do i=1,ndata
          argexp(i) = reflike(i) -sum(dt(:,i)*auxdt(:,i))
          alike(i)  = -.5d0*(argexp(i)+logdet(i))
       end do

    else

       argexp = 1.d30
       logdet = 1.d30

       alike = -.5d0*(argexp+logdet)
    endif

    if(debugging) then
       write(*,*) '--------------------------'
       do j=1,1
          write(*,*) 'argexp =',argexp(j)
          write(*,*) 'logdet =',logdet(j)
       end do
       write(*,*) '--------------------------'
    end if

    deallocate(tmplog,il,tm1,pinfo)
  end subroutine get_pix_loglike_pp

! ---------------------------------------------------------------------------

  subroutine update_ncvm(clsin,linf,lsup,NCM,project_mondip)
!input clsin are l(l+1)C_l/2pi

    implicit none
    
    real(dp)  ,intent(in)         :: clsin(2:,:)
    integer  ,intent(in)          :: linf ,lsup
    real(dp) ,intent(inout)       :: NCM(:,:)
    logical ,intent(in) ,optional :: project_mondip

    logical  :: pmd

    pmd = .false.
    if(present(project_mondip)) pmd = project_mondip

    if(ntemp .gt. 0) then
       if(debugging) then
          write(*,*) 'updating ncvm between '
          write(*,*) 'linf  = ',linf 
          write(*,*) 'lsup = ',lsup
          write(*,*) ''
          write(*,*) 'TT'
          write(*,*) 'NCVM(1,1) in  :',NCM(1,1)
          write(*,*) 'C_2^TT        :',clsin(2,myTT)
       end if
       call get_tt_cov(clsin,linf,lsup,NCM(1:ntemp,1:ntemp),project_mondip=pmd,symmetrize=.true.,update=.true.)
       if(debugging) then
          write(*,*) 'NCVM(1,1) out :',NCM(1,1)
       end if
    end if

    if(nqu .gt. 0) then
       if(debugging) then
          write(*,*) 'PP'
          write(*,*) 'NCVM(',ntemp+1,',',ntemp+1,') in  :',NCM(ntemp+1,ntemp+1)
          write(*,*) 'C_2^EE        :',clsin(2,myEE)
          write(*,*) 'C_2^BB        :',clsin(2,myBB)
          write(*,*) 'C_2^EB        :',clsin(2,myEB)
       end if
       call get_pp_cov(clsin, linf, lsup, NCM(1+ntemp:ntot,1+ntemp:ntot),&
            symmetrize=.true.,update=.true.)
       if(debugging) then
          write(*,*) 'NCVM(',ntemp+1,',',ntemp+1,') out :',NCM(ntemp+1,ntemp+1)
       end if
    end if

    if(ntemp .gt. 0 .and. nqu .gt.0) then 

       if(debugging) then
          write(*,*) 'TP'
          write(*,*) 'NCVM(',1,',',ntemp+2,') in  :',NCM(1,ntemp+2)
          write(*,*) 'NCVM(',ntemp+2,',',1,') in  :',NCM(ntemp+2,1)
          write(*,*) 'C_2^TE                      :',clsin(2,myTE)
          write(*,*) 'C_2^TB                      :',clsin(2,myTB)
       end if
       call get_xx_cov(clsin, linf, lsup, NCM, symmetrize=.true., &
            update=.true.)
       if(debugging) then
          write(*,*) 'NCVM(',1,',',ntemp+2,') out :',NCM(1,ntemp+2)
          write(*,*) 'NCVM(',ntemp+2,',',1,') out :',NCM(ntemp+2,1)
       end if
    end if
    
    return

  end subroutine update_ncvm

! ---------------------------------------------------------------------------

  pure subroutine get_rotation_angle(r1,r2,a12,a21)
    !computes TWICE the rotation angle

    implicit none
    real(dp) ,intent(in) ,dimension(3) :: r1,r2
    real(dp) ,intent(out)              :: a12
    real(dp) ,intent(out) ,optional    :: a21

    real(dp) ,parameter :: eps = 3.141592653589793d0/180.d0/3600.d0/100.d0
    real(dp) ,parameter ,dimension(3) :: zz =(/0,0,1/),epsilon =(/eps,0.d0,0.d0/)
    
    real(dp) ,dimension(3) :: r12,r1star,r2star
    real(dp)               :: mod

    call ext_prod(r1,r2,r12)
    mod = sqrt(sum(r12*r12))
    if(mod.lt.1.d-8) then !same or opposite pixels
       a12 = 0.d0
       if(present(a21)) a21 = 0.d0
       return
       
    end if
    r12 = r12/mod

    call ext_prod(zz,r1,r1star)
    r1star(3) = 0.d0
    mod = sqrt(sum(r1star*r1star))
    if(mod.lt.1.d-8) then   !r1 is at a pole            
       r1star = r1star+epsilon
       mod = sqrt(sum(r1star*r1star))
    end if
    r1star = r1star/mod

    call ext_prod(zz,r2,r2star)
    r2star(3) = 0.d0
    mod = sqrt(sum(r2star*r2star))
    if(mod.lt.1.d-8) then   !r2 is at a pole            
       r2star = r2star+epsilon
       mod = sqrt(sum(r2star*r2star))
    end if
    r2star = r2star/mod

    mod = sum(r12*r1star)
    mod = min(1.d0,mod)
    mod = max(-1.d0,mod)
    if(sum(r12*zz).gt.0.d0) then
       a12 = 2.d0*acos(mod)
    else
       a12 = -2.d0*acos(mod)
    end if

    a12 = -a12

    if(present(a21)) then
       r12 = -r12   !r21 = - r12
       mod = sum(r12*r2star)
       mod = min(1.d0,mod)
       mod = max(-1.d0,mod)
       if(sum(r12*zz).gt.0.d0) then
          a21 =  2.d0*acos(mod)
       else
          a21 = -2.d0*acos(mod)
       end if

       a21 = -a21
    end if

  end subroutine get_rotation_angle

! ---------------------------------------------------------------------------

  subroutine  get_QML_estimate(lsup,clsout,clserr)
!    use lpl_healpix_utils_mod

    implicit none
    integer(i4b) ,intent(in) :: lsup
    real(dp)    ,intent(out) :: clsout(2:,:,:) ,clserr(2:,:)

    integer(i4b)             :: xndata
    real(dp)    ,allocatable :: xout(:,:,:) ,xerr(:,:)
    
    clsout = 0._dp
    clserr = 0._dp
    if(has_x) then
       call get_QML_estimate_tp(lsup,clsout,clserr)
    elseif(has_t) then
       call get_QML_estimate_tt(lsup,clsout(2:,1,:),clserr(2:,1))
    else
       xndata =  size(clsout(2,1,:))
       allocate(xout(2:lsup,3,xndata) ,source = 0._dp)
       allocate(xerr(2:lsup,3)        ,source = 0._dp)
       call get_QML_estimate_pp(lsup,xout,xerr)
       clsout(:,myEE,:) = xout(:,1,:)
       clsout(:,myBB,:) = xout(:,2,:)
       clsout(:,myEB,:) = xout(:,3,:)
       clserr(:,myEE)   = xerr(:,1)
       clserr(:,myBB)   = xerr(:,2)
       clserr(:,myEB)   = xerr(:,3)
       deallocate(xout ,xerr)
    end if

  end subroutine get_QML_estimate
  
! ---------------------------------------------------------------------------

  subroutine  get_QML_estimate_tp(lsup,clsout,clserr)

    implicit none
    integer(i4b) ,intent(in) :: lsup
    real(dp)    ,intent(out) :: clsout(2:,:,:) ,clserr(2:,:)

    integer(i4b)                         :: info ,i ,n  
    integer(i4b)                         :: j1 ,j2 ,l1 ,l2 ,m1 ,nl1 ,nl2
    real(dp) ,allocatable,dimension(:,:) :: cls ,S0 ,auxmat ,auxmat2 ,auxmat3
    real(dp) ,allocatable,dimension(:,:) :: block1 ,block2
    real(dp) ,allocatable                :: FF(:,:,:,:) 
    type(allocatable_matrix) ,allocatable:: wevec(:) ,cevec(:)

    if(lsup .gt. lswitch) stop 'lsup must be <= lswitch'

    if(debugging) write(*,*) 'starting get_qml_estimate with lsup = ',lsup

    allocate(cls(2:lsup,6))

    if(decouple_tp) then
       allocate(S0,source = S)
       S(1:ntemp,ntemp+1:ntot) = 0._dp
       S(ntemp+1:ntot,1:ntemp) = 0._dp
       call  dpotrf('L',ntemp,S(1:ntemp,1:ntemp),ntemp,info)
       if(info.ne.0) then
          write(*,*) 'INFO =',info
          write(*,*) 'Cholesky for TT fails'
          stop
       end if

       call  dpotrf('L',nq+nu,S(ntemp+1:ntot,ntemp+1:ntot),nq+nu,info)
       if(info.ne.0) then
          write(*,*) 'INFO =',info
          write(*,*) 'Cholesky for Pol fails'
          stop
       end if
    else
       call  dpotrf('L',ntot,S,ntot,info)
       if(info.ne.0) then
          write(*,*) 'INFO =',info
          write(*,*) 'Cholesky for TP fails'
          stop
       end if
    end if

    !2E = C^-1 dC C^-1 = C^-1 Vt dB V C^-1 = Wt dB W

    !whiten all the evecs C^-1 Vt
    allocate(wevec(2:lsup),cevec(2:lsup))
    do l1=2,lsup
       nl1 = 3*(2*l1+1)
       call wevec(l1)%alloc(nrow=ntot,ncol=nl1)
       call cevec(l1)%alloc(nrow=ntot,ncol=nl1)
       wevec(l1)%m = fevec(l1)%m
       call dpotrs( 'L', ntot, nl1, S, ntot, wevec(l1)%m, ntot, INFO )
       if(decouple_tp) then
          call dsymm('L','L', ntot, nl1, 1.d0, S0, ntot, wevec(l1)%m, ntot, &
               0.d0, cevec(l1)%m, ntot)
       else
          cevec(l1)%m = fevec(l1)%m
       end if
    end do
    if(decouple_tp)  deallocate(S0)

    !get the raw estimates xEx -tr(NE)
    do l1=2,lsup
       nl1 = 3*(2*l1+1)
       !tr(NWt dB W) = tr(WNWt dB)
       !get (WNWt)
       !NWt
       allocate(auxmat,mold=wevec(l1)%m)
       call dgemm('N','N', ntot, nl1, ntot, 1.d0, N0, &
            ntot, wevec(l1)%m, ntot, 0.d0, auxmat, ntot)

       allocate(block1(nl1,nl1))
       !WNWt
       call dgemm('T','N', nl1, nl1, ntot, 1.d0, wevec(l1)%m, &
            ntot, auxmat, ntot, 0.d0, block1, nl1)
       deallocate(auxmat)
       
       allocate(auxmat(nl1,ndata),auxmat2(nl1,ndata))

       !Wx
       call dgemm('T','N', nl1, ndata, ntot, 1.d0, wevec(l1)%m, &
            ntot, dt, ntot, 0.d0, auxmat, nl1)

       allocate(block2(nl1,nl1))
       block2 = transpose(block1)

       do j1=1,6
          cls = 0._dp
          cls(l1,j1) = l1*(l1+1.d0)/twopi
          call cls2block(l1,cls,feval(l1)%m(:,1),cblocks(l1,:),block1)
          !tr(NE)
          do n = 1,nl1
             clsout(l1,j1,1) = clsout(l1,j1,1) - sum(block2(:,n)*block1(:,n))
          end do
          clsout(l1,j1,2:ndata) = clsout(l1,j1,1) 

          !xEx 
          call dgemm('N','N',nl1,ndata,nl1,1.d0,block1,nl1,auxmat,nl1, &
               0.d0,auxmat2,nl1)

          do i=1,ndata
             clsout(l1,j1,i) = clsout(l1,j1,i) +sum(auxmat(:,i)*auxmat2(:,i))
          end do
       end do
       deallocate(block1,block2,auxmat,auxmat2)
    end do
    clsout = clsout*0.5_dp 

    !compute fisher
    !F = tr(dC C^-1 dC C^-1)/2 = tr( Vt dB V C^-1 Vt dB V C^-1)/2
    allocate(FF(2:lsup,2:lsup,6,6))
    FF = 0.d0
    do l1 = 2,lsup
       nl1 = 3*(2*l1+1)
       allocate(block1(nl1,nl1))
       do j1 = 1,6
          cls = 0.d0
          cls(l1,j1) = l1*(l1+1.d0)/twopi
          call cls2block(l1,cls,feval(l1)%m(:,1),cblocks(l1,:),block1)

          do l2 = 2,lsup
             nl2 = 3*(2*l2+1)
             allocate(block2(nl2,nl2))
             allocate(auxmat(nl1,nl2) ,auxmat2(nl1,nl2) ,auxmat3(nl2,nl1))
             auxmat  =0.d0

             call dgemm('T','N', nl1, nl2, ntot, 1.d0, cevec(l1)%m, ntot, &
                  wevec(l2)%m, ntot, 0.d0, auxmat, nl1)

             call dgemm('N','N', nl1, nl2, nl1, 1.d0, block1, nl1, auxmat,&
                  nl1, 0.d0, auxmat2, nl1)

             do j2 = 1,6
                cls = 0.d0
                cls(l2,j2) = l2*(l2+1.d0)/twopi
                call cls2block(l2,cls,feval(l2)%m(:,1),cblocks(l2,:),block2)
                !B_2VW
                call dgemm('N','T', nl2, nl1, nl2, 1.d0, block2, nl2, &
                     auxmat, nl1, 0.d0, auxmat3, nl2)
                do m1 = 1,nl1
                   FF(l2,l1,j2,j1) = sum(auxmat2(m1,:)*auxmat3(:,m1)) &
                        + FF(l2,l1,j2,j1) 
                end do

             end do
             deallocate(auxmat,auxmat2,auxmat3)
             deallocate(block2)
          end do
       end do
       deallocate(block1)
    end do
    FF = FF*0.5_dp

    nl1 = lsup-1
    allocate(auxmat(6*nl1,6*nl1))
    do j1=1,6
       do j2=1,6
          auxmat((j2-1)*nl1 +1:j2*nl1,(j1-1)*nl1 +1:j1*nl1) = &
               FF(2:lsup,2:lsup,j2,j1)
       end do
    end do

    allocate(auxmat2(6*nl1,ndata))
    do n = 1,ndata
       do j1 = 1,6
          auxmat2((j1-1)*nl1 +1:j1*nl1,n) = clsout(2:lsup,j1,n)
       end do
    end do

    call dposv('L',6*nl1,ndata,auxmat,6*nl1,auxmat2,6*nl1,info)

    if(info .ne. 0) then
       write(*,*) 'TP Fisher matrix is not invertible info = ',info
       stop
    end if

    do n = 1,ndata
       do j1 = 1,6
          clsout(2:lsup,j1,n) = auxmat2((j1-1)*nl1 +1:j1*nl1,n) 
       end do
    end do
    call dpotri('L',6*nl1,auxmat,6*nl1,info)

    do j1 = 1,6
       do l1=2,lsup
          clserr(l1,j1) = sqrt(auxmat((j1-1)*nl1 +(l1-1),(j1-1)*nl1 +(l1-1)))
       end do
    end do

    deallocate(auxmat,auxmat2,FF)

    write(*,*) 'done get_qml_estimate'

  end subroutine get_QML_estimate_tp

! ---------------------------------------------------------------------------

  subroutine  get_QML_estimate_pp(lsup,clsout,clserr)

    implicit none
    integer(i4b) ,intent(in) :: lsup
    real(dp)    ,intent(out) :: clsout(2:,:,:) ,clserr(2:,:)

    integer(i4b)                         :: info ,i ,n  
    integer(i4b)                         :: j1 ,j2 ,l1 ,l2 ,m1 ,nl1 ,nl2
    real(dp) ,allocatable,dimension(:,:) :: cls ,auxmat ,auxmat2 ,auxmat3
    real(dp) ,allocatable,dimension(:,:) :: block1 ,block2
    real(dp) ,allocatable                :: FF(:,:,:,:) 
    type(allocatable_matrix) ,allocatable:: wevec(:) ,cevec(:)

    if(lsup .gt. lswitch) stop 'lsup must be <= lswitch'

    if(debugging) write(*,*) 'starting get_qml_estimate with lsup = ',lsup

    allocate(cls(2:lsup,3))

    call  dpotrf('L',ntot,S,ntot,info)
    if(info.ne.0) then
       write(*,*) 'INFO =',info
       write(*,*) 'Cholesky for TP fails'
       stop
    end if

    !2E = C^-1 dC C^-1 = C^-1 Vt dB V C^-1 = Wt dB W

    !whiten all the evecs C^-1 Vt
    allocate(wevec(2:lsup),cevec(2:lsup))
    do l1=2,lsup
       nl1 = 2*(2*l1+1)
       call wevec(l1)%alloc(nrow=ntot,ncol=nl1)
       call cevec(l1)%alloc(nrow=ntot,ncol=nl1)
       wevec(l1)%m = fevec(l1)%m
       call dpotrs( 'L', ntot, nl1, S, ntot, wevec(l1)%m, ntot, INFO )
       cevec(l1)%m = fevec(l1)%m
    end do

    !get the raw estimates xEx -tr(NE)
    do l1=2,lsup
       nl1 = 2*(2*l1+1)
       !tr(NWt dB W) = tr(WNWt dB)
       !get (WNWt)
       !NWt
       allocate(auxmat,mold=wevec(l1)%m)
       call dgemm('N','N', ntot, nl1, ntot, 1.d0, N0, &
            ntot, wevec(l1)%m, ntot, 0.d0, auxmat, ntot)

       allocate(block1(nl1,nl1) ,source = 0._dp)
       !WNWt
       call dgemm('T','N', nl1, nl1, ntot, 1.d0, wevec(l1)%m, &
            ntot, auxmat, ntot, 0.d0, block1, nl1)
       deallocate(auxmat)
       
       allocate(auxmat(nl1,ndata),auxmat2(nl1,ndata))

       !Wx
       call dgemm('T','N', nl1, ndata, ntot, 1.d0, wevec(l1)%m, &
            ntot, dt, ntot, 0.d0, auxmat, nl1)

       allocate(block2(nl1,nl1))
       block2 = transpose(block1)

       do j1=1,3
          cls = 0._dp
          cls(l1,j1) = l1*(l1+1.d0)/twopi
          call cls2block_p(l1,cls,feval(l1)%m(:,1),cblocks(l1,:),block1)
          !tr(NE)
          do n = 1,nl1
             clsout(l1,j1,1) = clsout(l1,j1,1) - sum(block2(:,n)*block1(:,n))
          end do
          clsout(l1,j1,2:ndata) = clsout(l1,j1,1) 

          !xEx 
          call dgemm('N','N',nl1,ndata,nl1,1.d0,block1,nl1,auxmat,nl1, &
               0.d0,auxmat2,nl1)

          do i=1,ndata
             clsout(l1,j1,i) = clsout(l1,j1,i) +sum(auxmat(:,i)*auxmat2(:,i))
          end do
       end do
       deallocate(block1,block2,auxmat,auxmat2)
    end do
    clsout = clsout*0.5_dp 

    !compute fisher
    !F = tr(dC C^-1 dC C^-1)/2 = tr( Vt dB V C^-1 Vt dB V C^-1)/2
    allocate(FF(2:lsup,2:lsup,3,3))
    FF = 0.d0
    do l1 = 2,lsup
       nl1 = 2*(2*l1+1)
       allocate(block1(nl1,nl1))
       do j1 = 1,3
          cls = 0.d0
          cls(l1,j1) = l1*(l1+1.d0)/twopi
          call cls2block_p(l1,cls,feval(l1)%m(:,1),cblocks(l1,:),block1)

          do l2 = 2,lsup
             nl2 = 2*(2*l2+1)
             allocate(block2(nl2,nl2))
             allocate(auxmat(nl1,nl2) ,auxmat2(nl1,nl2) ,auxmat3(nl2,nl1))
             auxmat  =0.d0

             call dgemm('T','N', nl1, nl2, ntot, 1.d0, cevec(l1)%m, ntot, &
                  wevec(l2)%m, ntot, 0.d0, auxmat, nl1)

             call dgemm('N','N', nl1, nl2, nl1, 1.d0, block1, nl1, auxmat,&
                  nl1, 0.d0, auxmat2, nl1)

             do j2 = 1,3
                cls = 0.d0
                cls(l2,j2) = l2*(l2+1.d0)/twopi
                call cls2block_p(l2,cls,feval(l2)%m(:,1),cblocks(l2,:),block2)
                !B_2VW
                call dgemm('N','T', nl2, nl1, nl2, 1.d0, block2, nl2, &
                     auxmat, nl1, 0.d0, auxmat3, nl2)
                do m1 = 1,nl1
                   FF(l2,l1,j2,j1) = sum(auxmat2(m1,:)*auxmat3(:,m1)) &
                        + FF(l2,l1,j2,j1) 
                end do

             end do
             deallocate(auxmat,auxmat2,auxmat3)
             deallocate(block2)
          end do
       end do
       deallocate(block1)
    end do
    FF = FF*0.5_dp

    nl1 = lsup-1
    allocate(auxmat(3*nl1,3*nl1))
    do j1=1,3
       do j2=1,3
          auxmat((j2-1)*nl1 +1:j2*nl1,(j1-1)*nl1 +1:j1*nl1) = &
               FF(2:lsup,2:lsup,j2,j1)
       end do
    end do

    allocate(auxmat2(3*nl1,ndata))
    do n = 1,ndata
       do j1 = 1,3
          auxmat2((j1-1)*nl1 +1:j1*nl1,n) = clsout(2:lsup,j1,n)
       end do
    end do

    call dposv('L',3*nl1,ndata,auxmat,3*nl1,auxmat2,3*nl1,info)

    if(info .ne. 0) then
       write(*,*) 'P Fisher matrix is not invertible info = ',info
       stop
    end if

    do n = 1,ndata
       do j1 = 1,3
          clsout(2:lsup,j1,n) = auxmat2((j1-1)*nl1 +1:j1*nl1,n) 
       end do
    end do
    call dpotri('L',3*nl1,auxmat,3*nl1,info)

    do j1 = 1,3
       do l1=2,lsup
          clserr(l1,j1) = sqrt(auxmat((j1-1)*nl1 +(l1-1),(j1-1)*nl1 +(l1-1)))
       end do
    end do

    deallocate(auxmat,auxmat2,FF)

    write(*,*) 'done get_qml_estimate'

  end subroutine get_QML_estimate_pp

! ---------------------------------------------------------------------------

  subroutine  get_QML_estimate_tt(lsup,clsout,clserr)

    implicit none
    integer(i4b) ,intent(in) :: lsup
    real(dp)    ,intent(out) :: clsout(2:,:) ,clserr(2:)

    integer(i4b)                         :: info ,i ,n  
    integer(i4b)                         :: l1 ,l2 ,m1 ,nl1 ,nl2
    real(dp) ,allocatable,dimension(:,:) :: auxmat ,auxmat2 ,auxmat3
    real(dp) ,allocatable,dimension(:,:) :: block1 ,block2
    real(dp) ,allocatable                :: FF(:,:) ,cls(:)
    type(allocatable_matrix) ,allocatable :: wevec(:) ,cevec(:)

    if(lsup .gt. lswitch) stop 'lsup must be <= lswitch'
    if(debugging) write(*,*) 'starting get_qml_estimate with lsup = ',lsup

    allocate(cls(2:lsup) ,source = 0._dp)

    call  dpotrf('L',ntot,S,ntot,info)
    if(info.ne.0) then
       write(*,*) 'INFO =',info
       write(*,*) 'Cholesky for TT fails'
       stop
    end if

    !2E = C^-1 dC C^-1 = C^-1 Vt dB V C^-1 = Wt dB W

    !whiten all the evecs C^-1 Vt
    allocate(wevec(2:lsup),cevec(2:lsup))
    do l1=2,lsup
       nl1 = 2*l1+1
       call wevec(l1)%alloc(nrow=ntot,ncol=nl1)
       call cevec(l1)%alloc(nrow=ntot,ncol=nl1)
       wevec(l1)%m = fevec(l1)%m
       call dpotrs( 'L', ntot, nl1, S, ntot, wevec(l1)%m, ntot, INFO )
       cevec(l1)%m = fevec(l1)%m
    end do

    !get the raw estimates xEx -tr(NE)
    do l1=2,lsup
       nl1 = 2*l1+1
       !tr(NWt dB W) = tr(WNWt dB)
       !get (WNWt)
       !NWt
       allocate(auxmat,mold=wevec(l1)%m)
       call dgemm('N','N', ntot, nl1, ntot, 1.d0, N0, &
            ntot, wevec(l1)%m, ntot, 0.d0, auxmat, ntot)
       
       allocate(block1(nl1,nl1) ,source = 0._dp)
       !WNWt
       call dgemm('T','N', nl1, nl1, ntot, 1.d0, wevec(l1)%m, &
            ntot, auxmat, ntot, 0.d0, block1, nl1)
       deallocate(auxmat)
       
       allocate(auxmat(nl1,ndata),auxmat2(nl1,ndata) ,source = 0._dp)

       !Wx
       call dgemm('T','N', nl1, ndata, ntot, 1.d0, wevec(l1)%m, &
            ntot, dt, ntot, 0.d0, auxmat, nl1)

       allocate(block2 ,mold = transpose(block1)) !workaround for gfortran issue
       block2 = transpose(block1)

       cls     = 0._dp
       cls(l1) = l1*(l1+1.d0)/twopi

       block1  = 0._dp
       do n = 1,nl1
          block1(n,n) = feval(l1)%m(n,1)*cls(l1)
       end do

       !tr(NE)
       do n = 1,nl1
          clsout(l1,1) = clsout(l1,1) - sum(block2(:,n)*block1(:,n))
       end do
       clsout(l1,2:ndata) = clsout(l1,1) 

       !xEx 
       call dgemm('N','N',nl1,ndata,nl1,1.d0,block1,nl1,auxmat,nl1, &
            0.d0,auxmat2,nl1)

       do i=1,ndata
          clsout(l1,i) = clsout(l1,i) +sum(auxmat(:,i)*auxmat2(:,i))
       end do
       deallocate(block1,block2,auxmat,auxmat2)
    end do
    clsout = clsout*0.5_dp 

    !compute fisher
    !F = tr(dC C^-1 dC C^-1)/2 = tr( Vt dB W Vt dB W )/2 =
    !  = tr(dB W Vt dB W Vt)/2
    allocate(FF(2:lsup,2:lsup) ,source = 0._dp)
    do l1 = 2,lsup
       nl1 = 2*l1+1
       allocate(block1(nl1,nl1) ,source = 0._dp)
       cls = 0.d0
       cls(l1) = l1*(l1+1.d0)/twopi
       do n = 1,nl1
          block1(n,n) = feval(l1)%m(n,1)*cls(l1)
       end do
       
       do l2 = l1,lsup
          nl2 = 2*l2+1
          allocate(block2(nl2,nl2) ,source = 0._dp)
          allocate(auxmat(nl1,nl2) ,auxmat2(nl1,nl2) ,auxmat3(nl2,nl1) &
               ,source = 0._dp)

          ! V_l1 W_l2 -> auxmat
          call dgemm('T','N', nl1, nl2, ntot, 1.d0, cevec(l1)%m, ntot, &
               wevec(l2)%m, ntot, 0.d0, auxmat, nl1)

          ! B_l1 V_l1 W_l2 -> auxmat2
          call dgemm('N','N', nl1, nl2, nl1, 1.d0, block1, nl1, auxmat,&
               nl1, 0.d0, auxmat2, nl1)

          cls = 0.d0
          cls(l2) = l2*(l2+1.d0)/twopi
          do n = 1,nl2
             block2(n,n) = feval(l2)%m(n,1)*cls(l2)
          end do
          !B_l2 V_l2 W_l1 = B_l2 (V_l1 W_l2)t ->auxmat3
          call dgemm('N','T', nl2, nl1, nl2, 1.d0, block2, nl2, &
               auxmat, nl1, 0.d0, auxmat3, nl2)

          do m1 = 1,nl1
             FF(l2,l1) = sum(auxmat2(m1,:)*auxmat3(:,m1)) &
                        + FF(l2,l1) 
          end do

          deallocate(auxmat,auxmat2,auxmat3)
          deallocate(block2)
       end do
       deallocate(block1)
    end do
    FF = FF*0.5_dp

    nl1 = lsup-1
    allocate(auxmat(nl1,nl1))
    auxmat(1:nl1,1:nl1) = FF(2:lsup,2:lsup)

    allocate(auxmat2(nl1,ndata) ,source = 0._dp)
    do n = 1,ndata
       auxmat2(1:nl1,n) = clsout(2:lsup,n)
    end do

    call dposv('L',nl1,ndata,auxmat,nl1,auxmat2,nl1,info)

    if(info .ne. 0) then
       write(*,*) 'T Fisher matrix is not invertible info = ',info
       stop
    end if

    do n = 1,ndata
       clsout(2:lsup,n) = auxmat2(1:nl1,n) 
    end do
    call dpotri('L',nl1,auxmat,nl1,info)

    do l1=2,lsup
       clserr(l1) = sqrt(auxmat(l1-1,l1-1))
    end do

    deallocate(auxmat,auxmat2,FF)

    write(*,*) 'done get_qml_estimate'

  end subroutine get_QML_estimate_tt

! ---------------------------------------------------------------------------

  subroutine  get_QMLM_estimate(lsup,modes,moderr,cov)

    implicit none
    integer(i4b) ,intent(in)  :: lsup
    real(dp)     ,intent(out) :: modes(:,:,:) ,moderr(:,:) ,cov(:,:)

    integer(i4b) :: info ,i ,n ,l1 ,l2 ,m1 ,nl0 ,nl1 ,nl2 ,imode1 ,imode2 &
         ,im1 ,im2 ,nmodes ,nmodes_tot ,imstart ,j1 ,j2 ,nl02

    real(dp) ,allocatable,dimension(:,:) :: cls ,S0 ,auxmat ,auxmat2 ,auxmat3
    real(dp) ,allocatable,dimension(:,:) :: block1 ,block2
    real(dp) ,allocatable                :: FF(:,:,:,:) 

    type(allocatable_matrix) ,allocatable :: wevec(:) ,cevec(:)

    if(lsup .gt. lswitch) stop 'lsup must be <= lswitch'

    if(debugging) write(*,*) 'starting get_qml_estimate with lsup = ',lsup

    modes  = 0._dp
    nmodes = (lsup+1)**2 -4

    if(decouple_tp) then
       allocate(S0,source = S)
       S(1:ntemp,ntemp+1:ntot) = 0._dp
       S(ntemp+1:ntot,1:ntemp) = 0._dp
       call  dpotrf('L',ntemp,S(1:ntemp,1:ntemp),ntemp,info)
       if(info.ne.0) then
          write(*,*) 'INFO =',info
          write(*,*) 'Cholesky for TT fails'
          stop
       end if

       call  dpotrf('L',nq+nu,S(ntemp+1:ntot,ntemp+1:ntot),nq+nu,info)
       if(info.ne.0) then
          write(*,*) 'INFO =',info
          write(*,*) 'Cholesky for Pol fails'
          stop
       end if
    else
       call  dpotrf('L',ntot,S,ntot,info)
       if(info.ne.0) then
          write(*,*) 'INFO =',info
          write(*,*) 'Cholesky for TP fails'
          stop
       end if
    end if

    !2E = C^-1 dC C^-1 = C^-1 Vt dB V C^-1 = Wt dB W

    !whiten all the evecs C^-1 Vt
    allocate(wevec(2:lsup),cevec(2:lsup))
    do l1=2,lsup
       nl1 = 3*(2*l1+1)
       call wevec(l1)%alloc(nrow=ntot,ncol=nl1)
       call cevec(l1)%alloc(nrow=ntot,ncol=nl1)
       wevec(l1)%m = fevec(l1)%m
       call dpotrs( 'L', ntot, nl1, S, ntot, wevec(l1)%m, ntot, INFO )
       if(decouple_tp) then
          call dsymm('L','L', ntot, nl1, 1.d0, S0, ntot, wevec(l1)%m, ntot, &
               0.d0, cevec(l1)%m, ntot)
       else
          cevec(l1)%m = fevec(l1)%m
       end if
    end do
    if(decouple_tp)  deallocate(S0)

    !get the raw estimates xEx -tr(NE)
    imode1 = 0
    do l1=2,lsup
       nl0 = 2*l1 +1 
       nl1 = 3*nl0

       !tr(NWt dB W) = tr(WNWt dB)
       !get (WNWt)
       !NWt
       allocate(auxmat,mold=wevec(l1)%m)
       call dgemm('N','N', ntot, nl1, ntot, 1.d0, N0, &
            ntot, wevec(l1)%m, ntot, 0.d0, auxmat, ntot)

       allocate(block1(nl1,nl1))
       !WNWt
       call dgemm('T','N', nl1, nl1, ntot, 1.d0, wevec(l1)%m, &
            ntot, auxmat, ntot, 0.d0, block1, nl1)
       deallocate(auxmat)
       
       allocate(auxmat(nl1,ndata),auxmat2(nl1,ndata))

       !Wx
       call dgemm('T','N', nl1, ndata, ntot, 1.d0, wevec(l1)%m, &
            ntot, dt, ntot, 0.d0, auxmat, nl1)

       allocate(block2(nl1,nl1))
       block2 = transpose(block1)

       do im1 = 1,nl0
          imode1 = imode1 +1
          do j1 = 1,6
             call cls2block_modes(l1,im1,j1,feval(l1)%m(:,1),cblocks(l1,:)&
               ,block1)

             !tr(NE)
             do n = 1,nl1
                modes(imode1,j1,1) = modes(imode1,j1,1) - sum(block2(:,n)*block1(:,n))
             end do
             modes(imode1,j1,2:ndata) = modes(imode1,j1,1) 

             !xEx 
             call dgemm('N','N',nl1,ndata,nl1,1.d0,block1,nl1,auxmat,nl1, &
                  0.d0,auxmat2,nl1)
             
             do i=1,ndata
                modes(imode1,j1,i) = modes(imode1,j1,i) +sum(auxmat(:,i)*auxmat2(:,i))
             end do
          end do
       end do
       deallocate(block1,block2,auxmat,auxmat2)
    end do
    modes = modes*0.5_dp 

    !compute fisher
    !F = tr(dC C^-1 dC C^-1)/2 = tr( Vt dB W Vt dB W )/2 =
    !  = tr(dB W Vt dB W Vt)/2
    allocate(FF(nmodes,nmodes,6,6) ,source = 0._dp)

    do j1 = 1,6
       do l1 = 2,lsup
          nl0 = 2*l1 +1
          nl1 = 3*nl0
          allocate(block1(nl1,nl1) ,source = 0._dp)

          do im1 = 1,nl0
             call lm2mode(l1,im1,imode1)

             call cls2block_modes(l1,im1,j1,feval(l1)%m(:,1),cblocks(l1,:),block1)
             do j2 = j1,6
                do l2 = l1,lsup
                   nl02 = 2*l2 +1
                   nl2  = 3*nl02
                   allocate(block2(nl2,nl2) ,source = 0._dp)
                   allocate(auxmat(nl1,nl2) ,auxmat2(nl1,nl2) &
                        ,auxmat3(nl2,nl1) ,source = 0._dp)

                   call dgemm('T','N', nl1, nl2, ntot, 1.d0, cevec(l1)%m, ntot, &
                        wevec(l2)%m, ntot, 0.d0, auxmat, nl1)

                   call dgemm('N','N', nl1, nl2, nl1, 1.d0, block1, nl1, auxmat,&
                        nl1, 0.d0, auxmat2, nl1)

                   do im2 = 1,nl02
                      call lm2mode(l2,im2,imode2)

                      call cls2block_modes(l2 ,im2 ,j2 ,feval(l2)%m(:,1) &
                           ,cblocks(l2,:) ,block2)

                      call dgemm('N','T', nl2, nl1, nl2, 1.d0, block2, nl2, &
                           auxmat, nl1, 0.d0, auxmat3, nl2)

                      do m1 = 1,nl1
                         FF(imode2,imode1,j2,j1) = FF(imode2,imode1,j2,j1)+ &
                              sum(auxmat2(m1,:)*auxmat3(:,m1)) 
                      end do
                   end do
                   deallocate(auxmat,auxmat2,auxmat3,block2)
                end do
             end do
          end do
          deallocate(block1)
       end do
    end do
    do j1 = 1,6
       do j2 = j1,6
          FF(:,:,j2,j1) = FF(:,:,j2,j1)/2
          call symmetrize_matrix(FF(:,:,j2,j1))
          FF(:,:,j1,j2) = transpose(FF(:,:,j2,j1))
       end do
    end do

    nl1 = nmodes
    allocate(auxmat(6*nl1,6*nl1) ,source = 0._dp)
    do j1=1,6
       do j2=1,6
          auxmat((j2-1)*nl1 +1:j2*nl1,(j1-1)*nl1 +1:j1*nl1) = &
               FF(:,:,j2,j1)
       end do
    end do

    allocate(auxmat2(6*nl1,ndata))
    do n = 1,ndata
       do j1 = 1,6
          auxmat2((j1-1)*nl1 +1:j1*nl1,n) = modes(1:nmodes,j1,n)
       end do
    end do

    call dposv('L',6*nl1,ndata,auxmat,6*nl1,auxmat2,6*nl1,info)
    if(info .ne. 0) then
       write(*,*) 'Fisher matrix is not invertible info = ',info
       stop
    end if
    do n = 1,ndata
       do j1 = 1,6
          modes(1:nl1,j1,n) = auxmat2((j1-1)*nl1 +1:j1*nl1,n)
       end do
    end do
    
    call dpotri('L',6*nmodes,auxmat,6*nmodes,info)
    cov = auxmat
    do j1 = 1,6
       do im1 = 1,nl1
          im2 = im1 +(j1-1)*nl1
          moderr(im1,j1) = sqrt(auxmat(im1,im1))
       !symmetrize covmat
!       cov(im1,im1+1:3*nmodes) = cov(im1+1:3*nmodes,im1)

       end do
    end do

    deallocate(auxmat,auxmat2,FF)

    write(*,*) 'done get_QMLM_estimate'

  contains

    subroutine lm2mode(l,m,i)
      implicit none
      integer(i4b) ,intent(in)  :: l ,m
      integer(i4b) ,intent(out) :: i

      i = l**2 +m -4

    end subroutine lm2mode
    
    subroutine mode2lm(i,l,m)
      implicit none
      integer(i4b) ,intent(in)  :: i
      integer(i4b) ,intent(out) :: l ,m

      l = int(sqrt(real(i+4+tiny(1._dp),dp)),i4b)
      m = i +4 -l**2
      
    end subroutine mode2lm
    
  end subroutine get_QMLM_estimate

! ---------------------------------------------------------------------------  

  subroutine  cls2block(l,cls,eval,crossterm,mat)
    implicit none
    integer(i4b) ,intent(in) :: l
    real(dp) ,intent(in)     :: cls(2:,1:) ,eval(:) 
    type(allocatable_matrix) ,intent(in) :: crossterm(2:)
    real(dp) ,intent(out)    :: mat(:,:)

    integer                  :: nl ,nl0 ,i 

    nl0 = 3*(2*l+1)
    nl = size(eval(:))
    if(nl .ne. nl0) then
       write(*,*) 'eigenvalues vector has size ',nl,' requested number of modes',nl0
       stop
    end if

    nl = size(mat(:,1))
    if(nl .ne. nl0) then
       write(*,*) 'total matrix has size ',nl,' requested number of modes ',nl0
       stop
    end if

    nl  = 2*l+1
    mat = 0._dp

    do i=1,nl
       mat(i,i) = eval(i)*cls(l,myTT)
    end do

    do i =2,6
       mat = mat +cls(l,i)*crossterm(i)%m
    end do

  end subroutine cls2block

! ---------------------------------------------------------------------------
 
  subroutine  cls2block_p(l,cls,eval,crossterm,mat)
    implicit none
    integer(i4b) ,intent(in) :: l
    real(dp) ,intent(in)     :: cls(2:,1:) ,eval(:) 
    type(allocatable_matrix) ,intent(in) :: crossterm(2:)
    real(dp) ,intent(out)    :: mat(:,:)

    integer                  :: nl ,nl0 

    nl0 = 2*(2*l+1)
    nl = size(eval(:))
    if(nl .ne. nl0) then
       write(*,*) 'eigenvalues vector has size ',nl,' requested number of modes ',nl0
       stop
    end if

    nl = size(mat(:,1))
    if(nl .ne. nl0) then
       write(*,*) 'total matrix has size ',nl,' requested number of modes ',nl0
       stop
    end if

    nl = 2*l+1

    mat = 0._dp
    mat = mat +cls(l,1)*crossterm(myEE)%m
    mat = mat +cls(l,2)*crossterm(myBB)%m
    mat = mat +cls(l,3)*crossterm(myEB)%m

  end subroutine cls2block_p

! ---------------------------------------------------------------------------

  subroutine  cls2block_modes(l,imode,spectra,eval,crossterm,mat)
    !only works for TT, EE & BB for now
    implicit none
    integer(i4b) ,intent(in) :: l ,imode ,spectra 
    real(dp)     ,intent(in) :: eval(:) 
    type(allocatable_matrix) ,intent(in) :: crossterm(2:)
    real(dp) ,intent(out)    :: mat(:,:)

    integer                  :: nl ,nl0 ,indx
    real(dp)                 :: norm


    norm = l*(l+1)/twopi
    
    nl0 = 3*(2*l+1)
    nl  = size(eval(:))
    if(nl .ne. nl0) then
       write(*,*) 'eigenvalues vector has size ',nl,' requested number of modes ',nl0
       stop
    end if

    nl = size(mat(:,1))
    if(nl .ne. nl0) then
       write(*,*) 'total matrix has size ',nl,' requested number of modes ',nl0
       stop
    end if

    nl  = 2*l+1
    mat = 0._dp

    select case (spectra)
    case (1,2,3) ! TT, EE, BB diagonal
       indx = (spectra-1)*nl +imode
       mat(indx,indx) = eval(indx)*norm
    case (myTE)  
       mat(nl+imode,imode) = norm*crossterm(myTE)%m(nl+imode,imode)
       mat(imode,nl+imode) = norm*crossterm(myTE)%m(nl+imode,imode)
    case (myTB)  
       mat(2*nl+imode,imode) = norm*crossterm(myTB)%m(2*nl+imode,imode)
       mat(imode,2*nl+imode) = norm*crossterm(myTB)%m(2*nl+imode,imode)
    case (myEB)  
       mat(2*nl+imode,nl+imode) = norm*crossterm(myEB)%m(2*nl+imode,nl+imode)
       mat(nl+imode,2*nl+imode) = norm*crossterm(myEB)%m(2*nl+imode,nl+imode)
    end select
       
  end subroutine cls2block_modes

! ---------------------------------------------------------------------------

  subroutine precompute_rotation_angle(s1,c1,s2,c2)

    implicit none
    real(dp) ,intent(out) :: s1(:,:) ,c1(:,:) ,s2(:,:) ,c2(:,:)  

    integer(i4b)          :: i ,j ,iq ,jq ,iu ,ju
    real(dp)              :: a1 ,a2
       
! QT
!$OMP PARALLEL DEFAULT (PRIVATE) SHARED(c1,c2,s1,s2,Qvec,Tvec,Uvec,ntemp,nq,nu,ntot)
!$OMP DO
    do i=1,ntemp
       do j=1,nq
          call get_rotation_angle(QVec(:,j),TVec(:,i),a1,a2)
          jq = j +ntemp
          c1(jq,i) = cos(a1)
          s1(jq,i) = sin(a1)
          c2(jq,i) = cos(a2)
          s2(jq,i) = sin(a2)

       enddo
!UT
       do j=1,nu
          call get_rotation_angle(UVec(:,j),TVec(:,i),a1,a2)
          ju = j +ntemp +nq
          c1(ju,i) = cos(a1)
          s1(ju,i) = sin(a1)
          c2(ju,i) = cos(a2)
          s2(ju,i) = sin(a2)
       end do
    end do

!$OMP DO
    do i = 1,nq
!TQ       
       iq = i+ntemp
       do j=1,ntemp
          call get_rotation_angle(TVec(:,j),QVec(:,i),a1,a2)
          c1(j,iq) = cos(a1)
          s1(j,iq) = sin(a1)
          c2(j,iq) = cos(a2)
          s2(j,iq) = sin(a2)
       end do
!QQ
       do j = 1,nq
          call get_rotation_angle(QVec(:,j),QVec(:,i),a1,a2)
          jq = j+ntemp

          c1(jq,iq) = cos(a1)
          s1(jq,iq) = sin(a1)
          c2(jq,iq) = cos(a2)
          s2(jq,iq) = sin(a2)

       end do
!UQ
       do j=1,nu
          call get_rotation_angle(UVec(:,j),QVec(:,i),a1,a2)

          ju = j+ntemp+nq

          c1(ju,iq) = cos(a1)
          s1(ju,iq) = sin(a1)
          c2(ju,iq) = cos(a2)
          s2(ju,iq) = sin(a2)

       end do
    end do

!$OMP DO
    do i =1,nu
!TU
       iu = i+ntemp+nq
       do j=1,ntemp
          call get_rotation_angle(TVec(:,j),UVec(:,i),a1,a2)
          c1(j,iu) = cos(a1)
          s1(j,iu) = sin(a1)
          c2(j,iu) = cos(a2)
          s2(j,iu) = sin(a2)
       end do
!QU
       do j = 1,nq
          jq = j+ntemp
          call get_rotation_angle(QVec(:,j),UVec(:,i),a1,a2)

          c1(jq,iu) = cos(a1)
          s1(jq,iu) = sin(a1)
          c2(jq,iu) = cos(a2)
          s2(jq,iu) = sin(a2)

       end do
!UU
       do j=i,nu

          call get_rotation_angle(UVec(:,j),UVec(:,i),a1,a2)

          ju = j+ntemp+nq

          c1(ju,iu) = cos(a1)
          s1(ju,iu) = sin(a1)
          c2(ju,iu) = cos(a2)
          s2(ju,iu) = sin(a2)

       end do
    end do
!$OMP END PARALLEL
    
  end subroutine precompute_rotation_angle

end module bflike_smw_mod



